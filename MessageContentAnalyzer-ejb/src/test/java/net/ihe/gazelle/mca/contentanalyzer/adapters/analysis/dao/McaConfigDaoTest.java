package net.ihe.gazelle.mca.contentanalyzer.adapters.analysis.dao;

import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.mca.contentanalyzer.adapters.config.dao.McaConfigDaoImpl;
import net.ihe.gazelle.mca.contentanalyzer.business.model.Analysis;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.*;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ EntityManager.class, EntityManagerService.class })
@PowerMockIgnore({ "org.xml.*", "org.w3c.*", "javax.xml.*" })
public class McaConfigDaoTest {

    McaConfigDaoImpl mcaConfigDaoSpied;

    EntityManagerFactory entityManagerFactory;
    EntityManager entityManager;

    @Before
    public void setUp() {
        mockEntityManager();
        mcaConfigDaoSpied = Mockito.spy(new McaConfigDaoImpl(entityManagerFactory));
        when(mcaConfigDaoSpied.getConfigsFromDB()).thenReturn(new McaConfigDaoStub().getConfigsFromDB());
    }

    private void mockEntityManager() {
        entityManagerFactory = Mockito.mock(EntityManagerFactory.class);
        when(entityManagerFactory.createEntityManager()).thenReturn(entityManager);
        entityManager = PowerMockito.mock(EntityManager.class);
        PowerMockito.mockStatic(EntityManagerService.class);
        PowerMockito.when(EntityManagerService.provideEntityManager()).thenReturn(entityManager);
        when(entityManager.merge(Mockito.anyObject())).thenAnswer(new Answer<Analysis>() {
            @Override
            public Analysis answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                Analysis analysis = (Analysis) args[0];
                analysis.setId(10);
                return analysis;
            }
        });
    }

    @Ignore
    @Test
    public void testMergeConfigs() {

        ConfigsFromDB importedConfigs = new ConfigsFromDB();
        int nbrNewConfigs = 0;
        ConfigsFromDB overwrittenConfigs = new ConfigsFromDB();
        nbrNewConfigs = mcaConfigDaoSpied.addNewImportedConfiguration(importedConfigs, overwrittenConfigs);
        assertEquals(0, nbrNewConfigs);
        assertEquals(0, overwrittenConfigs.getXmlTagConfigEntities().size());
        assertEquals(0, overwrittenConfigs.getContentAnalysisConfigEntities().size());
        assertEquals(0, overwrittenConfigs.getMimeTypeConfigEntities().size());

        importedConfigs.setContentAnalysisConfigEntities(new ArrayList<ContentAnalysisConfigEntity>());
        importedConfigs.setMimeTypeConfigEntities(new ArrayList<MimeTypeConfigEntity>());
        importedConfigs.setXmlTagConfigEntities(new ArrayList<XmlTagConfigEntity>());

        nbrNewConfigs = mcaConfigDaoSpied.addNewImportedConfiguration(importedConfigs, overwrittenConfigs);
        assertEquals(0, nbrNewConfigs);
        assertEquals(0, overwrittenConfigs.getXmlTagConfigEntities().size());
        assertEquals(0, overwrittenConfigs.getContentAnalysisConfigEntities().size());
        assertEquals(0, overwrittenConfigs.getMimeTypeConfigEntities().size());

        importedConfigs.getContentAnalysisConfigEntities().add(new ContentAnalysisConfigEntity("HTTP", null,
                Arrays.asList("GET", "HEAD", "POST", "PUT", "DELETE", "TRACE", "CONNECT"),
                null, null, null));
        importedConfigs.getMimeTypeConfigEntities().add(new MimeTypeConfigEntity("application/pdf", "PDF", ValidationType.PDF));
        importedConfigs.getXmlTagConfigEntities().add(new XmlTagConfigEntity("Envelope", "http://www.w3.org/2003/05/soap-envelope","SOAP Envelope", null));

        nbrNewConfigs = mcaConfigDaoSpied.addNewImportedConfiguration(importedConfigs, overwrittenConfigs);
        assertEquals(0, nbrNewConfigs);
        assertEquals(0, overwrittenConfigs.getXmlTagConfigEntities().size());
        assertEquals(0, overwrittenConfigs.getContentAnalysisConfigEntities().size());
        assertEquals(0, overwrittenConfigs.getMimeTypeConfigEntities().size());

        importedConfigs.getContentAnalysisConfigEntities().add(new ContentAnalysisConfigEntity("TEST", null,
                Arrays.asList("TEST", "TEST", "TEST"), null, null, null));
        importedConfigs.getMimeTypeConfigEntities().add(new MimeTypeConfigEntity("application/test", "TEST", ValidationType.PDF));
        importedConfigs.getXmlTagConfigEntities().add(new XmlTagConfigEntity("test", "http://test","TEST", null));

        nbrNewConfigs = mcaConfigDaoSpied.addNewImportedConfiguration(importedConfigs, overwrittenConfigs);
        assertEquals(3, nbrNewConfigs);
        assertEquals(0, overwrittenConfigs.getXmlTagConfigEntities().size());
        assertEquals(0, overwrittenConfigs.getContentAnalysisConfigEntities().size());
        assertEquals(0, overwrittenConfigs.getMimeTypeConfigEntities().size());

        importedConfigs.setContentAnalysisConfigEntities(new ArrayList<ContentAnalysisConfigEntity>());
        importedConfigs.setMimeTypeConfigEntities(new ArrayList<MimeTypeConfigEntity>());
        importedConfigs.setXmlTagConfigEntities(new ArrayList<XmlTagConfigEntity>());

        importedConfigs.getContentAnalysisConfigEntities().add(new ContentAnalysisConfigEntity("NewDocTypeToOverwrite", null,
                Arrays.asList("GET", "HEAD", "POST", "PUT", "DELETE", "TRACE", "CONNECT"),
                null, null, null));
        importedConfigs.getMimeTypeConfigEntities().add(new MimeTypeConfigEntity("application/pdf", "NewDocTypeToOverwrite", ValidationType.PDF));
        importedConfigs.getXmlTagConfigEntities().add(new XmlTagConfigEntity("Envelope", "http://www.w3.org/2003/05/soap-envelope","NewDocTypeToOverwrite", null));

        nbrNewConfigs = mcaConfigDaoSpied.addNewImportedConfiguration(importedConfigs, overwrittenConfigs);
        assertEquals(0, nbrNewConfigs);
        assertEquals(1, overwrittenConfigs.getXmlTagConfigEntities().size());
        assertEquals(1, overwrittenConfigs.getContentAnalysisConfigEntities().size());
        assertEquals(1, overwrittenConfigs.getMimeTypeConfigEntities().size());

        importedConfigs.setContentAnalysisConfigEntities(new ArrayList<ContentAnalysisConfigEntity>());
        importedConfigs.setMimeTypeConfigEntities(new ArrayList<MimeTypeConfigEntity>());
        importedConfigs.setXmlTagConfigEntities(new ArrayList<XmlTagConfigEntity>());

        importedConfigs.getContentAnalysisConfigEntities().add(new ContentAnalysisConfigEntity("HTTP", ValidationType.WS_TRUST,
                Arrays.asList("GET", "HEAD", "POST", "PUT", "DELETE", "TRACE", "CONNECT"),
                null, null, null));
        importedConfigs.getMimeTypeConfigEntities().add(new MimeTypeConfigEntity("application/pdf", "PDF", ValidationType.XML));
        importedConfigs.getXmlTagConfigEntities().add(new XmlTagConfigEntity("Envelope", "http://www.w3.org/2003/05/soap-envelope","SOAP Envelope", ValidationType.HL7V3));
        overwrittenConfigs = new ConfigsFromDB();

        nbrNewConfigs = mcaConfigDaoSpied.addNewImportedConfiguration(importedConfigs, overwrittenConfigs);
        assertEquals(0, nbrNewConfigs);
        assertEquals(1, overwrittenConfigs.getXmlTagConfigEntities().size());
        assertEquals(1, overwrittenConfigs.getContentAnalysisConfigEntities().size());
        assertEquals(1, overwrittenConfigs.getMimeTypeConfigEntities().size());
    }

    @Ignore
    @Test
    public void testMergeOnCurrentConfig() {

        ConfigsFromDB importedConfigs = mcaConfigDaoSpied.getConfigsFromDB();
        int nbrNewConfigs = 0;
        ConfigsFromDB overwrittenConfigs = new ConfigsFromDB();

        nbrNewConfigs = mcaConfigDaoSpied.addNewImportedConfiguration(importedConfigs, overwrittenConfigs);
        assertEquals(0, nbrNewConfigs);
        assertEquals(0, overwrittenConfigs.getXmlTagConfigEntities().size());
        assertEquals(0, overwrittenConfigs.getContentAnalysisConfigEntities().size());
        assertEquals(0, overwrittenConfigs.getMimeTypeConfigEntities().size());

    }
}
