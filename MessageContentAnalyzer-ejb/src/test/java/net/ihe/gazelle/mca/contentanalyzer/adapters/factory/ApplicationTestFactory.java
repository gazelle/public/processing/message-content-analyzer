package net.ihe.gazelle.mca.contentanalyzer.adapters.factory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.mca.contentanalyzer.application.analysis.AnalysisManager;
import net.ihe.gazelle.mca.contentanalyzer.application.analysis.AnalysisPartManager;
import net.ihe.gazelle.mca.contentanalyzer.application.analyzers.*;
import net.ihe.gazelle.mca.contentanalyzer.application.analyzers.xml.TagDetector;
import net.ihe.gazelle.mca.contentanalyzer.application.analyzers.xml.XmlAnalyzer;
import net.ihe.gazelle.mca.contentanalyzer.application.config.McaConfigManager;
import net.ihe.gazelle.mca.contentanalyzer.application.converters.Base64Converter;
import net.ihe.gazelle.mca.contentanalyzer.application.converters.DicomToTxtConverter;
import net.ihe.gazelle.mca.contentanalyzer.application.files.FilesDownloaderManager;
import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.McaConfigDao;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.McaConfigDaoStub;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.mockito.Mockito.when;

@Name("mcaApplicationFactory")
@Scope(ScopeType.APPLICATION)
public class ApplicationTestFactory {

    @Mock
    AnalysisManager analysisManager = new AnalysisManager(null,null,null,null,null);

    @Mock
    DicomToTxtConverter dicomToTxtConverter;

    @Mock
    Base64Converter base64Converter;

    @Mock
    AnalysisPartManager analysisPartManager;

    @Mock
    ApplicationPreferenceManager applicationPreferenceManager;

    private final McaConfigDao mcaConfigDao = new McaConfigDaoStub();



    public TagDetector getTagDetector() {
        return new TagDetector(mcaConfigDao,getMessageContentAnalyzer());
    }

    public XmlAnalyzer getXmlAnalyzer() {
        return new XmlAnalyzer(mcaConfigDao);
    }

    public ContentAnalysisTypeDetector getContentAnalysisTypeDetector() {
        return new ContentAnalysisTypeDetector(getMessageSplitter(), mcaConfigDao);
    }

    public MimeTypeDetector getMimeTypeDetector() {
        if(applicationPreferenceManager == null) {
            mockApplicationPreferenceManager();
        }
        return new MimeTypeDetector(applicationPreferenceManager, mcaConfigDao);
    }

    public B64Analyzer getB64Analyzer() {
        return new B64Analyzer(getXmlAnalyzer(), getContentAnalysisTypeDetector(),
                getMimeTypeDetector(), new Base64Converter());
    }

    public MessageContentAnalyzer getMessageContentAnalyzer() {
        return new MessageContentAnalyzer(getXmlAnalyzer(), getContentAnalysisTypeDetector(),
                getMimeTypeDetector(), getB64Analyzer());
    }

    public MessageSplitter getMessageSplitter() {
        // No dependency injection in test
        MessageSplitter messageSplitter = new MessageSplitter(getXmlAnalyzer(), null,
                getMimeTypeDetector(), null);
        messageSplitter.setContentAnalysisTypeDetector(new ContentAnalysisTypeDetector(messageSplitter, mcaConfigDao));
        messageSplitter.setB64Analyzer(getB64AnalyzerWithoutContentAnalysis(messageSplitter));
        return messageSplitter;
    }

    public B64Analyzer getB64AnalyzerWithoutContentAnalysis(MessageSplitter messageSplitter) {

        B64Analyzer b64Analyzer = new B64Analyzer(getXmlAnalyzer(), null,
                getMimeTypeDetector(), new Base64Converter());
        b64Analyzer.setContentAnalysisTypeDetector(new ContentAnalysisTypeDetector(messageSplitter, mcaConfigDao));
        return b64Analyzer;
    }

    public McaConfigManager getMcaConfigManager(){
        return new McaConfigManager(new McaConfigDaoStub());
    }

    public FilesDownloaderManager getFilesDownloaderManager() {
        return new FilesDownloaderManager(analysisPartManager, dicomToTxtConverter);
    }

    public AnalysisManager getAnalysisManager() {
        return analysisManager;
    }

    public DicomToTxtConverter getDicomToTxtConverter() {
        return dicomToTxtConverter;
    }

    public Base64Converter getBase64Converter() {
        return base64Converter;
    }

    private void mockApplicationPreferenceManager() {
        applicationPreferenceManager = Mockito.mock(ApplicationPreferenceManager.class);
        when(applicationPreferenceManager.getBooleanValue("mca_show_zip_mimtype"))
                .thenReturn(true);
    }
}
