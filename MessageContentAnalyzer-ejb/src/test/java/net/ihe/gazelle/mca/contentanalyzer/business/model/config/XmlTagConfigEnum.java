package net.ihe.gazelle.mca.contentanalyzer.business.model.config;

import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.XmlNodeMatcher;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.XmlTagConfigInterface;

import javax.persistence.Transient;
import javax.xml.xpath.XPathExpressionException;

public enum XmlTagConfigEnum implements XmlTagConfigInterface {

    ENVELOPE("Envelope", "http://www.w3.org/2003/05/soap-envelope","SOAP Envelope", null),
    BODY("Body", "http://www.w3.org/2003/05/soap-envelope", "SOAP Body", null),
    HEADER("Header", "http://www.w3.org/2003/05/soap-envelope", "SOAP Header", null),
    CLINICAL_DOCUMENT("ClinicalDocument", "urn:hl7-org:v3", "CDA", ValidationType.CDA),
    HL7V3("([A-Z]{4}_[A-Z]{2}[0-9]{6})([A-Z]{2}[0-9]{2})", "urn:hl7-org:v3", "HL7V3", ValidationType.HL7V3),
    SUBSCRIBE("Subscribe", "http://docs.oasis-open.org/wsn/b-2", "DSUB", ValidationType.DSUB),
    SUBSCRIBE_RESPONSE("SubscribeResponse", "http://docs.oasis-open.org/wsn/b-2", "DSUB", ValidationType.DSUB),
    UNSUBSCRIBE("Unsubscribe", "http://docs.oasis-open.org/wsn/b-2", "DSUB", ValidationType.DSUB),
    UNSUBSCRIBE_RESPONSE("UnsubscribeResponse", "http://docs.oasis-open.org/wsn/b-2", "DSUB", ValidationType.DSUB),
    NOTIFY("Notify", "http://docs.oasis-open.org/wsn/b-2", "DSUB", ValidationType.DSUB),
    CREATE_PULL_POINT("CreatePullPoint", "http://docs.oasis-open.org/wsn/b-2", "DSUB", ValidationType.DSUB),
    CREATE_PULL_POINT_RESPONSE("CreatePullPointResponse", "http://docs.oasis-open.org/wsn/b-2", "DSUB", ValidationType.DSUB),
    DESTROY_PULL_POINT("DestroyPullPoint", "http://docs.oasis-open.org/wsn/b-2", "DSUB", ValidationType.DSUB),
    DESTROY_PULL_POINT_RESPONSE("DestroyPullPointResponse", "http://docs.oasis-open.org/wsn/b-2", "DSUB", ValidationType.DSUB),
    GET_MESSAGES("GetMessages", "http://docs.oasis-open.org/wsn/b-2", "DSUB", ValidationType.DSUB),
    GET_MESSAGES_RESPONSE("GetMessagesResponse", "http://docs.oasis-open.org/wsn/b-2", "DSUB", ValidationType.DSUB),
    PROVIDE_AND_REGISTER_DOCUMENT_SET_REQUEST("ProvideAndRegisterDocumentSetRequest", "urn:ihe:iti:xds-b:2007", "XDS", ValidationType.XDS),
    RETRIEVE_DOCUMENT_SET_REQUEST("RetrieveDocumentSetRequest", "urn:ihe:iti:xds-b:2007", "XDS", ValidationType.XDS),
    RETRIEVE_DOCUMENT_SET_RESPONSE("RetrieveDocumentSetResponse", "urn:ihe:iti:xds-b:2007", "XDS", ValidationType.XDS),
    RETRIEVE_IMAGING_DOCUMENT_SET_REQUEST("RetrieveImagingDocumentSetRequest", null, "XDS", ValidationType.XDS),
    CATALOG_CONTENT_REQUEST("CatalogContentRequest", null, "XDS", ValidationType.XDS),
    CATALOG_CONTENT_RESPONSE("CatalogContentResponse", null, "XDS", ValidationType.XDS),
    VALIDATE_CONTENT_REQUEST("ValidateContentRequest", null, "XDS", ValidationType.XDS),
    VALIDATE_CONTENT_RESPONSE("ValidateContentResponse", null, "XDS", ValidationType.XDS),
    ACCEPT_OBJECTS_REQUEST("AcceptObjectsRequest", null, "XDS", ValidationType.XDS),
    APPROVE_OBJECTS_REQUEST("ApproveObjectsRequest", null, "XDS", ValidationType.XDS),
    DEPRECATED_OBJECTS_REQUEST("DeprecateObjectsRequest", null, "XDS", ValidationType.XDS),
    RELOCATE_OBJECTS_REQUEST("RelocateObjectsRequest", null, "XDS", ValidationType.XDS),
    REMOVE_OBJECTS_REQUEST("RemoveObjectsRequest", null, "XDS", ValidationType.XDS),
    SUBMIT_OBJECTS_REQUEST("SubmitObjectsRequest", null, "XDS", ValidationType.XDS),
    UNDEPRECATED_OBJECTS_REQUEST("UndeprecateObjectsRequest", null, "XDS", ValidationType.XDS),
    UPDATE_OBJECTS_REQUEST("UpdateObjectsRequest", null, "XDS", ValidationType.XDS),
    ADHOC_QUERY_QUERY("AdhocQueryQuery", null, "XDS", ValidationType.XDS),
    ADHOC_QUERY_REQUEST("AdhocQueryRequest", "urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0", "XDS", ValidationType.XDS),
    ADHOC_QUERY_RESPONSE("AdhocQueryResponse", "urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0", "XDS", ValidationType.XDS),
    ASSOCIATION_QUERY("AssociationQuery", null, "XDS", ValidationType.XDS),
    AUDITABLE_EVENT_QUERY("AuditableEventQuery", null, "XDS", ValidationType.XDS),
    CLASSIFICATION_NODE_QUERY("ClassificationNodeQuery", null, "XDS", ValidationType.XDS),
    CLASSIFICATION_QUERY("ClassificationQuery", null, "XDS", ValidationType.XDS),
    CLASSIFICATION_SCHEME_QUERY("ClassificationSchemeQuery", null, "XDS", ValidationType.XDS),
    EXTERNAL_IDENTIFIER_QUERY("ExternalIdentifierQuery", null, "XDS", ValidationType.XDS),
    EXTERNAL_LINK_QUERY("ExternalLinkQuery", null, "XDS", ValidationType.XDS),
    EXTRINSIC_OBJECT_QUERY("ExtrinsicObjectQuery", null, "XDS", ValidationType.XDS),
    FEDERATION_QUERY("FederationQuery", null, "XDS", ValidationType.XDS),
    NOTIFICATION_QUERY("NotificationQuery", null, "XDS", ValidationType.XDS),
    ORGANIZATION_QUERY("OrganizationQuery", null, "XDS", ValidationType.XDS),
    PERSON_QUERY("PersonQuery", null, "XDS", ValidationType.XDS),
    REGISTRY_PACKAGE_QUERY("RegistryPackageQuery", null, "XDS", ValidationType.XDS),
    REGISTRY_QUERY("RegistryQuery", null, "XDS", ValidationType.XDS),
    SERVICE_BINDING_QUERY("ServiceBindingQuery", null, "XDS", ValidationType.XDS),
    SERVICE_QUERY("ServiceQuery", null, "XDS", ValidationType.XDS),
    SPECIFICATION_LINK_QUERY("SpecificationLinkQuery", null, "XDS", ValidationType.XDS),
    SUBSCRIPTION_QUERY("SubscriptionQuery", null, "XDS", ValidationType.XDS),
    USER_QUERY("UserQuery", null, "XDS", ValidationType.XDS),
    ADHOC_QUERY("AdhocQuery", null, "XDS", ValidationType.XDS),
    QUERY_EXPRESSION("QueryExpression", null, "XDS", ValidationType.XDS),
    REGISTRY_REQUEST("RegistryRequest", null, "XDS", ValidationType.XDS),
    REGISTRY_RESPONSE("RegistryResponse", "urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0", "XDS", ValidationType.XDS),
    XDW_WORKFLOW_DOCUMENT("XDW.WorkflowDocument", "urn:ihe:iti:2011:xdw", "XDW", ValidationType.XDW),
    AUDIT_MESSAGE("AuditMessage", null, "AUDIT_MESSAGE", ValidationType.AUDIT_MESSAGE),
    BATCH_REQUEST("batchRequest", "urn:oasis:names:tc:DSML:2:0:core", "HPD", ValidationType.HPD),
    BATCH_RESPONSE("batchResponse", null, "HPD", ValidationType.HPD),
    RETRIEVE_VALUE_SET_RESPONSE("RetrieveValueSetResponse", null, "SVS", ValidationType.SVS),
    RETRIEVE_MULTIPLE_VALUE_SET_RESPONSE("RetrieveMultipleValueSetsResponse", null, "SVS", ValidationType.SVS),
    RETRIEVE_VALUE_SET_REQUEST("RetrieveValueSetRequest", null, "SVS", ValidationType.SVS),
    RETRIEVE_MULTIPLE_VALUE_SET_REQUEST("RetrieveMultipleValueSetsRequest", "urn:ihe:iti:svs:2008", "SVS", ValidationType.SVS),
    XACML_AUTHZ_DECISION_QUERY("XACMLAuthzDecisionQuery", "urn:oasis:names:tc:xacml:2.0:context:schema:os", "CH:ADR", ValidationType.XML),
    XACML_AUTHZ_DECISION_QUERY2("XACMLAuthzDecisionQuery", "urn:oasis:names:tc:xacml:2.0:profile:saml2.0:v2:schema:protocol", "CH:ADR", ValidationType.XML),
    ADD_POLICY_REQUEST("AddPolicyRequest", "urn:e-health-suisse:2015:policy-administration", "CH:PPQ-1", ValidationType.XML),
    UPDATE_POLICY_REQUEST("UpdatePolicyRequest", "urn:e-health-suisse:2015:policy-administration", "CH:PPQ-1", ValidationType.XML),
    EPR_POLICY_REPOSITORY_RESPONSE("EprPolicyRepositoryResponse", "urn:e-health-suisse:2015:policy-administration", "CH:PPQ-1", ValidationType.XML),
    DELETE_POLICY_REQUEST("DeletePolicyRequest", "urn:e-health-suisse:2015:policy-administration", "CH:PPQ-1", ValidationType.XML),
    UNKNOWN_POLICY_SET_ID("UnknownPolicySetId", "urn:e-health-suisse:2015:policy-administration", "CH:PPQ-1", ValidationType.XML),
    XACML_POLICY_QUERY("XACMLPolicyQuery", "urn:oasis:names:tc:xacml:2.0:profile:saml2.0:v2:schema:protocol", "CH:PPQ-2", ValidationType.XML),
    XACML_RESPONSE("Response", "urn:oasis:names:tc:SAML:2.0:protocol", "XACML Response", ValidationType.XML),
    FAULT("Fault", null, "SOAP Fault", null),
    REQUEST_SECURITY_TOKEN("RequestSecurityToken", "http://docs.oasis-open.org/ws-sx/ws-trust/200512", "WSTrust", ValidationType.XML),
    REQUEST_SECURITY_TOKEN_RESPONSE_COLLECTION("RequestSecurityTokenResponseCollection", "http://docs.oasis-open.org/ws-sx/ws-trust/200512", "WSTrust", ValidationType.XML),
    ASSERTION("Assertion", "urn:oasis:names:tc:SAML:2.0:assertion", "SAML", ValidationType.SAML),
    EMBEDDED_PDF("<xpath tags=\"(\\w+:)?(value|text)$\">./*[local-name()='value'][@mediaType='application/pdf'][@representation='B64']|./*[local-name()='text'][@mediaType='application/pdf'][@representation='B64']", "urn:hl7-org:v3", "B64PDF", null),
    EMBEDDED_X505("<xpath tags=\"(\\w+:)?X509Certificate$\">./*[local-name()='X509Certificate']", "http://www.w3.org/2000/09/xmldsig#", "B64X509", null),
    //EMBEDDED_DSIG("<xpath tags=\"(\\w+:)?SignatureValue$\">./*[local-name()='SignatureValue']", "http://www.w3.org/2000/09/xmldsig#", "B64DSIG", null)
    ;


    private final String tag;
    private final String namespace;
    private final String docType;
    private final ValidationType validationType;
    @Transient
    private XmlNodeMatcher matcher;

    XmlTagConfigEnum(final String tag, final String namespace, final String docType, final ValidationType validationType) {
        this.tag = tag;
        this.namespace = namespace;
        this.docType = docType;
        this.validationType = validationType;
        this.matcher = XmlTagConfigEntity.buildMatcher(this.tag,this.namespace);
    }

    public String getTag() {
        return tag;
    }

    public String getNamespace() {
        return namespace;
    }

    public String getDocType() {
        return docType;
    }

    public ValidationType getValidationType() {
        return validationType;
    }

    @Override
    public boolean hasSameDetectionElements(XmlTagConfigInterface xmlTagConfigInterface) {
        if ((xmlTagConfigInterface.getNamespace() == null && this.namespace == null)) {
            return xmlTagConfigInterface.getTag().equals(this.tag);
        } else if (this.namespace != null && xmlTagConfigInterface.getNamespace() != null) {
            return (xmlTagConfigInterface.getTag().equals(this.tag)
                    && xmlTagConfigInterface.getNamespace().equals(this.namespace));
        }
        return false;
    }

    @Override
    public XmlNodeMatcher getMatcher() {
        return matcher;
    }

    @Override
    public String getSample() {
        return null;
    }

    @Override
    public boolean isHardCoded() {
        return false;
    }

    @Override
    public ConfigType getConfigType() {
        return ConfigType.TAG_CONFIG;
    }
}
