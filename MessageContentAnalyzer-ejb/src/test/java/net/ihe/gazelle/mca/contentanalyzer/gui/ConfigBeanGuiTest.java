package net.ihe.gazelle.mca.contentanalyzer.gui;

import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.mca.contentanalyzer.adapters.config.gui.ConfigBeanGui;
import net.ihe.gazelle.mca.contentanalyzer.adapters.factory.ApplicationTestFactory;
import net.ihe.gazelle.mca.contentanalyzer.application.converters.ConfigToXmlConverter;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.ConfigType;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.ConfigsFromDB;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.ContentAnalysisConfigEntity;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.McaConfigDaoStub;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.ConfigInterface;
import org.jboss.seam.Component;
import org.jboss.seam.faces.FacesMessages;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ EntityManager.class, EntityManagerService.class, FacesMessages.class, FacesContext.class, Component.class })
@PowerMockIgnore({ "org.xml.*", "org.w3c.*", "javax.xml.*" })
public class ConfigBeanGuiTest {

    McaConfigDaoStub mcaConfigDaoStub;
    ConfigBeanGui configBeanGui;

    @Before
    public void setUp() throws Exception{

        ApplicationTestFactory applicationTestFactory = new ApplicationTestFactory();
        configBeanGui = new ConfigBeanGui(applicationTestFactory.getMcaConfigManager(), applicationTestFactory.getFilesDownloaderManager());
        mcaConfigDaoStub = new McaConfigDaoStub();
        mockEntityManager();
        mockFacesMessages();
    }

    private void mockEntityManager() {
        EntityManager em = PowerMockito.mock(EntityManager.class);
        PowerMockito.mockStatic(EntityManagerService.class);
        PowerMockito.when(EntityManagerService.provideEntityManager()).thenReturn(em);
    }

    private void mockFacesMessages() throws Exception {
        FacesMessages facesMessageMock = PowerMockito.mock(FacesMessages.class);
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(Component.class);
        PowerMockito.when(FacesMessages.instance()).thenReturn(facesMessageMock);
        // TODO
/*
        McaPersistenceManagerImpl mcaPersistenceManager = PowerMockito.mock(McaPersistenceManagerImpl.class);

        PowerMockito.whenNew(McaPersistenceManagerImpl.class).withNoArguments().thenReturn(mcaPersistenceManager);
        PowerMockito.when(mcaPersistenceManager.persistAnalysisData(any(Analysis.class), any(byte[].class),
                any(OIDGenerator.class), any(ValidationCacheManager.class))).thenAnswer(new Answer<Analysis>() {
            @Override
            public Analysis answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                Analysis analysis = (Analysis) args[0];
                analysis.setId(10);
                analysis.setFilePath("FilePath");
                return analysis;
            }
        });
 */
    }

    @Test
    public void testByteArrayToHex() {

        ContentAnalysisConfigEntity contentAnalysisConfigEntity = new ContentAnalysisConfigEntity();
        contentAnalysisConfigEntity.setBytePattern(new byte[]{0,0});
        configBeanGui.setSelectedConfig(contentAnalysisConfigEntity);
        assertEquals("0000", configBeanGui.getSelectedBytePattern());
        configBeanGui.getSelectedContentAnalysisConfig().setBytePattern(null);
        configBeanGui.computeSelectedBytePatternToHexString();
        assertNull(configBeanGui.getSelectedBytePattern());
    }

    @Test
    public void testListConfig() {
        configBeanGui.listConfigs();
        assertEquals(89, configBeanGui.getConfigs().newQueryBuilder().getList().size());
    }

    @Test
    public void testManipulateStartElements() {
        ContentAnalysisConfigEntity contentAnalysisConfigEntity = new ContentAnalysisConfigEntity();
        configBeanGui.setSelectedConfig(contentAnalysisConfigEntity);
        configBeanGui.setDisplayedStartsWith("Test");
        configBeanGui.saveStartsWith();
        assertEquals("Test", configBeanGui.getSelectedStartsWith());
        Assert.assertEquals(1, configBeanGui.getSelectedContentAnalysisConfig().getStartsWith().size());
        configBeanGui.editStartsWith();
        assertEquals("Test", configBeanGui.getSelectedStartsWith());
        assertEquals("Test", configBeanGui.getDisplayedStartsWith());
        configBeanGui.setDisplayedStartsWith("Test2");
        configBeanGui.saveStartsWith();
        assertEquals("Test2", configBeanGui.getSelectedStartsWith());
        Assert.assertEquals(1, configBeanGui.getSelectedContentAnalysisConfig().getStartsWith().size());
        configBeanGui.newStartsWith();
        assertEquals(null, configBeanGui.getSelectedStartsWith());
        assertEquals("", configBeanGui.getDisplayedStartsWith());
        configBeanGui.setDisplayedStartsWith("Test3");
        configBeanGui.saveStartsWith();
        assertEquals("Test3", configBeanGui.getSelectedStartsWith());
        Assert.assertEquals(2, configBeanGui.getSelectedContentAnalysisConfig().getStartsWith().size());
        Assert.assertEquals("Test2", configBeanGui.getSelectedContentAnalysisConfig().getStartsWith().get(0));
        Assert.assertEquals("Test3", configBeanGui.getSelectedContentAnalysisConfig().getStartsWith().get(1));
    }

    @Test
    public void testConfigToXmlConverter() throws Exception{
        ConfigToXmlConverter configToXmlConverter = new ConfigToXmlConverter();

        McaConfigDaoStub mcaConfigDAO = new McaConfigDaoStub();
        List<ConfigInterface> interfaceList = new ArrayList<>();
        interfaceList.addAll(mcaConfigDAO.getListOfContentAnalysisConfigEntity());
        interfaceList.addAll(mcaConfigDAO.getListOfMimeTypeEntity());
        interfaceList.addAll(mcaConfigDAO.getListOfXmlTagConfigEntity());
        String result = configToXmlConverter.configToXml(interfaceList);

        ConfigsFromDB importedConfig = configToXmlConverter.xmlToConfig(result);

        assertEquals(6, importedConfig.getContentAnalysisConfigEntities().size());
        assertEquals("DICOM", importedConfig.getContentAnalysisConfigEntities().get(0).getDocType());
        Assert.assertEquals(ValidationType.DICOM, importedConfig.getContentAnalysisConfigEntities().get(0).getValidationType());
        assertEquals(null, importedConfig.getContentAnalysisConfigEntities().get(0).getStartsWith());


        byte[] bytePattern = new byte[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 68, 73, 67, 77};

        assertTrue(Arrays.equals(bytePattern, importedConfig.getContentAnalysisConfigEntities().get(0).getBytePattern()));

        assertEquals(null, importedConfig.getContentAnalysisConfigEntities().get(0).getStringPattern());
        assertEquals(null, importedConfig.getContentAnalysisConfigEntities().get(0).getUnwantedContent());

        assertEquals(2, importedConfig.getMimeTypeConfigEntities().size());
        assertEquals("application/pdf", importedConfig.getMimeTypeConfigEntities().get(0).getMimeType());
        assertEquals("PDF", importedConfig.getMimeTypeConfigEntities().get(0).getDocType());
        Assert.assertEquals(ValidationType.PDF, importedConfig.getMimeTypeConfigEntities().get(0).getValidationType());

        assertEquals(8, importedConfig.getXmlTagConfigEntities().size());
        assertEquals("Envelope", importedConfig.getXmlTagConfigEntities().get(0).getTag());
        assertEquals("http://www.w3.org/2003/05/soap-envelope", importedConfig.getXmlTagConfigEntities().get(0).getNamespace());
        assertEquals("SOAP Envelope", importedConfig.getXmlTagConfigEntities().get(0).getDocType());
        Assert.assertEquals(null, importedConfig.getXmlTagConfigEntities().get(0).getValidationType());

    }

    @Test
    public void testContentAnalysisConfigMatch() {

        List<ContentAnalysisConfigEntity> contentAnalysisConfigEntityList = mcaConfigDaoStub.getListOfContentAnalysisConfigEntity();
        ContentAnalysisConfigEntity customEntity = new ContentAnalysisConfigEntity();
        customEntity.setBytePattern(new byte[]{0, 0, 0, 0, 0, 1, 2, 3, 4, 6});
        customEntity.setUnwantedContent("fromage");
        customEntity.setStringPattern("Pattern");
        customEntity.setStartsWith(Arrays.asList("Start", "Element", "For", "The", "Entity"));

        for (ContentAnalysisConfigEntity contentAnalysisConfigEntity : contentAnalysisConfigEntityList) {
            assertFalse(contentAnalysisConfigEntity.match(customEntity.getStartsWith(), customEntity.getBytePattern(),
                    customEntity.getStringPattern(), customEntity.getUnwantedContent()));
        }
        assertTrue(new ContentAnalysisConfigEntity().match(null, null, null, null));
    }

    @Test
    public void testContentAnalysisConfigMatchOnExistingConfigs() {

        List<ContentAnalysisConfigEntity> contentAnalysisConfigEntityList = mcaConfigDaoStub.getListOfContentAnalysisConfigEntity();

        for (ContentAnalysisConfigEntity configToCompare : contentAnalysisConfigEntityList) {
            boolean matched = false;
            for (ContentAnalysisConfigEntity contentAnalysisConfigEntity : contentAnalysisConfigEntityList) {
                if (contentAnalysisConfigEntity.match(configToCompare.getStartsWith(), configToCompare.getBytePattern(),
                        configToCompare.getStringPattern(), configToCompare.getUnwantedContent())) {
                    matched = true;
                }
            }
            assertTrue(matched);
        }
    }

    @Test
    @Ignore("This test is ignored because it needs a ResourceBundle mock for I18n")
    public void testSaveSelectedContentAnalysisConfig() {
        configBeanGui.setSelectedConfig(new ContentAnalysisConfigEntity());
        configBeanGui.setSelectedBytePattern("0123456789abcdefABCDEF");
        configBeanGui.saveSelectedConfig();

        assertNotNull(configBeanGui.getSelectedContentAnalysisConfig().getBytePattern());

        configBeanGui.setSelectedConfig(new ContentAnalysisConfigEntity());
        configBeanGui.setSelectedBytePattern("ZZZZZ");
        configBeanGui.saveSelectedConfig();

        assertNull(configBeanGui.getSelectedContentAnalysisConfig().getBytePattern());
    }

    @Test
    public void testListConfigsFiltered() {

        configBeanGui.setSelectedValidationTypeFilter(null);
        configBeanGui.listConfigs();
        assertEquals(89, configBeanGui.getConfigs().newQueryBuilder().getList().size());
        configBeanGui.setFilteredConfigType(ConfigType.CONTENT_ANALYSIS_CONFIGURATION);
        configBeanGui.listConfigs();
        assertEquals(6, configBeanGui.getConfigs().newQueryBuilder().getList().size());
        configBeanGui.setFilteredConfigType(ConfigType.MIME_TYPE_CONFIG);
        configBeanGui.listConfigs();
        assertEquals(2, configBeanGui.getConfigs().newQueryBuilder().getList().size());
        configBeanGui.setFilteredConfigType(ConfigType.TAG_CONFIG);
        configBeanGui.listConfigs();
        assertEquals(81, configBeanGui.getConfigs().newQueryBuilder().getList().size());


        configBeanGui.setSelectedValidationTypeFilter("Show All");
        configBeanGui.setFilteredConfigType(null);
        configBeanGui.listConfigs();
        assertEquals(89, configBeanGui.getConfigs().newQueryBuilder().getList().size());
        configBeanGui.setFilteredConfigType(ConfigType.CONTENT_ANALYSIS_CONFIGURATION);
        configBeanGui.listConfigs();
        assertEquals(6, configBeanGui.getConfigs().newQueryBuilder().getList().size());
        configBeanGui.setFilteredConfigType(ConfigType.MIME_TYPE_CONFIG);
        configBeanGui.listConfigs();
        assertEquals(2, configBeanGui.getConfigs().newQueryBuilder().getList().size());
        configBeanGui.setFilteredConfigType(ConfigType.TAG_CONFIG);
        configBeanGui.listConfigs();
        assertEquals(81, configBeanGui.getConfigs().newQueryBuilder().getList().size());


        configBeanGui.setSelectedValidationTypeFilter("N/A");
        configBeanGui.setFilteredConfigType(null);
        configBeanGui.listConfigs();
        assertEquals(3, configBeanGui.getConfigs().newQueryBuilder().getList().size());
        configBeanGui.setFilteredConfigType(ConfigType.CONTENT_ANALYSIS_CONFIGURATION);
        configBeanGui.listConfigs();
        assertEquals(1, configBeanGui.getConfigs().newQueryBuilder().getList().size());
        configBeanGui.setFilteredConfigType(ConfigType.MIME_TYPE_CONFIG);
        configBeanGui.listConfigs();
        assertEquals(1, configBeanGui.getConfigs().newQueryBuilder().getList().size());
        configBeanGui.setFilteredConfigType(ConfigType.TAG_CONFIG);
        configBeanGui.listConfigs();
        assertEquals(1, configBeanGui.getConfigs().newQueryBuilder().getList().size());

        configBeanGui.setSelectedValidationTypeFilter(ValidationType.ATNA.getValue());
        configBeanGui.setFilteredConfigType(null);
        configBeanGui.listConfigs();
        assertEquals(3, configBeanGui.getConfigs().newQueryBuilder().getList().size());
        configBeanGui.setFilteredConfigType(ConfigType.CONTENT_ANALYSIS_CONFIGURATION);
        configBeanGui.listConfigs();
        assertEquals(1, configBeanGui.getConfigs().newQueryBuilder().getList().size());
        configBeanGui.setFilteredConfigType(ConfigType.MIME_TYPE_CONFIG);
        configBeanGui.listConfigs();
        assertEquals(1, configBeanGui.getConfigs().newQueryBuilder().getList().size());
        configBeanGui.setFilteredConfigType(ConfigType.TAG_CONFIG);
        configBeanGui.listConfigs();
        assertEquals(1, configBeanGui.getConfigs().newQueryBuilder().getList().size());
    }

}
