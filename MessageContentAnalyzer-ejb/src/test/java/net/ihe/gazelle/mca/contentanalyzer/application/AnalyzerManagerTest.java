package net.ihe.gazelle.mca.contentanalyzer.application;


import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.CallerMetadataFactory;
import net.ihe.gazelle.evsclient.application.OIDGeneratorManagerImpl;
import net.ihe.gazelle.evsclient.application.interfaces.ForbiddenAccessException;
import net.ihe.gazelle.evsclient.application.interfaces.NotFoundException;
import net.ihe.gazelle.evsclient.application.interfaces.ProcessingCacheManager;
import net.ihe.gazelle.evsclient.application.interfaces.UnauthorizedException;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.NotLoadedException;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingNotSavedException;
import net.ihe.gazelle.evsclient.domain.processing.*;
import net.ihe.gazelle.evsclient.interlay.dao.HandledObjectFile;
import net.ihe.gazelle.evsclient.interlay.gui.document.ContentConverter;
import net.ihe.gazelle.evsclient.interlay.servlet.Upload;
import net.ihe.gazelle.mca.contentanalyzer.adapters.analysis.dao.AnalysisDaoImpl;
import net.ihe.gazelle.mca.contentanalyzer.adapters.analysis.dao.AnalysisPartDaoImpl;
import net.ihe.gazelle.mca.contentanalyzer.adapters.analysis.gui.AnalysisBeanGui;
import net.ihe.gazelle.mca.contentanalyzer.adapters.config.QueryParamMca;
import net.ihe.gazelle.mca.contentanalyzer.adapters.factory.ApplicationTestFactory;
import net.ihe.gazelle.mca.contentanalyzer.application.analysis.AnalysisManager;
import net.ihe.gazelle.mca.contentanalyzer.application.analysis.AnalysisPartManager;
import net.ihe.gazelle.mca.contentanalyzer.application.analyzers.AnalyzerManager;
import net.ihe.gazelle.mca.contentanalyzer.application.analyzers.MessageContentAnalyzer;
import net.ihe.gazelle.mca.contentanalyzer.application.config.McaConfigManager;
import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.DecodedPartNotSavedException;
import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.McaConfigDao;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import net.ihe.gazelle.mca.contentanalyzer.business.model.Analysis;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisStatus;
import net.ihe.gazelle.mca.contentanalyzer.business.model.EncodedType;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.McaConfigDaoStub;
import net.ihe.gazelle.metadata.application.MetadataServiceProvider;
import net.ihe.gazelle.metadata.application.ServiceBuilder;
import net.ihe.gazelle.metadata.domain.Service;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import org.apache.commons.io.FileUtils;
import org.jboss.seam.Component;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.contexts.Lifecycle;
import org.jboss.seam.faces.FacesMessages;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static net.ihe.gazelle.files.FilesUtils.loadFile;
import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ FacesMessages.class, Component.class, FacesContext.class, ExternalContext.class, Lifecycle.class, Contexts.class, Upload.class })
@PowerMockIgnore({ "org.xml.*", "org.w3c.*", "javax.xml.*" })
public class AnalyzerManagerTest {

    private static final String EXISTING_OID = "1.3.6.1.4.1.12559.11.1.2.1.4.1";
    private static final String EXISTING_ID_CONTENT = "existing1ID";
    public static final String EXISTING_OID_CONTENT = "existingOID";
    public static final String NOT_EXISTING_OID = "notExistingOID";
    public static final String NOT_EXISTING_ID = "0";
    public static final int ANALYSIS_ID = 69;

    @Mock
    AnalysisDaoImpl analysisDao;
    @Mock
    AnalysisPartDaoImpl analysisPartDao;
    @Mock
    ApplicationPreferenceManager applicationPreferenceManager;
    @Mock
    MetadataServiceProvider metadataServiceProvider;
    @Mock
    OIDGeneratorManagerImpl oidGeneratorManager;
    @Mock
    GazelleIdentity gazelleIdentity;
    @Mock
    ProcessingCacheManager processingCacheManager;
    @Mock
    CallerMetadataFactory callerMetadataFactory;

    AnalysisManager analysisManager;

    AnalysisManager analysisManagerSpied;

    AnalysisPartManager analysisPartManager;

    McaApi mcaApi;

    AnalyzerManager analyzerManager;

    AnalysisBeanGui analysisBeanGui;

    AnalysisPart analysisPart;
    Analysis analysis;

    MessageContentAnalyzer m;
    McaConfigDao mcaConfigDao;

    @After
    public void tearDown() {
        m = null;
    }

    private Analysis getDummyAnalysis() {
        AnalysisPart analysisPart;
        Analysis analysis;

        HandledObjectFile object = new HandledObjectFile(EXISTING_OID_CONTENT.getBytes(), null);
        object.setFilePath("filepath");

        analysis = new Analysis(object,
                new EVSCallerMetadata(EntryPoint.GUI));
        AnalysisPart analysisPartRoot = new AnalysisPart();
        analysisPartRoot.setOid(EXISTING_OID_CONTENT);
        analysisPartRoot.setDocType("txt");
        analysisPart = new AnalysisPart(analysisPartRoot);
        analysisPart.setAnalysis(analysis);
        analysisPart.setDocType("txt");
        analysisPartRoot.setChildPart(Arrays.asList(analysisPart));
        analysis.setRootAnalysisPart(analysisPartRoot);
        return analysis;
    }

    @Before
    public void setUp() throws Exception {

        m = new ApplicationTestFactory().getMessageContentAnalyzer();
        mcaConfigDao = new McaConfigDaoStub();

        MockitoAnnotations.initMocks(this);
        mockFacesMessages(QueryParamMca.PROCESSING_OID, EXISTING_OID_CONTENT);
        oidGeneratorManager = Mockito.mock(OIDGeneratorManagerImpl.class);
        when(oidGeneratorManager.getNewOid()).thenReturn(EXISTING_OID);



        analysis = getDummyAnalysis();

        mockAnalysisPartDao();
        mockAnalysisDao();

        gazelleIdentity = Mockito.mock(GazelleIdentity.class);
        when(gazelleIdentity.isLoggedIn()).thenReturn(false);

        processingCacheManager = Mockito.mock(ProcessingCacheManager.class);

        callerMetadataFactory = Mockito.mock(CallerMetadataFactory.class);
        when(callerMetadataFactory.getCallerMetadata(Matchers.<String>any(), Matchers.<String>any(), Matchers.<String>any(), Matchers.<EntryPoint>any()))
                .thenReturn(null);

        mockApplicationPreferenceManager();

        analysisManager = new AnalysisManager(analysisDao, applicationPreferenceManager, oidGeneratorManager,
                metadataServiceProvider, processingCacheManager);

        analysisManagerSpied = Mockito.spy(analysisManager);
        mockAnalysisManager();

        mockMcaApi();
        mockUpload();

        analysisPartManager = new AnalysisPartManager(analysisPartDao);

        analyzerManager = new AnalyzerManager(mcaApi, analysisManagerSpied,
                analysisPartManager, new McaConfigManager(new McaConfigDaoStub()));

        analysisBeanGui = new AnalysisBeanGui(analysisManagerSpied, analysisPartManager,
                new McaConfigManager(new McaConfigDaoStub()), mcaApi, analyzerManager,
                callerMetadataFactory, null, null, applicationPreferenceManager, gazelleIdentity);
    }

    private void mockUpload() {
        PowerMockito.mockStatic(Upload.class);
        when(Upload.putFile(Matchers.<String>any())).thenReturn("keyFilePath");
    }

    private void mockMcaApi() throws UnexpectedAnalysisException {
        mcaApi = Mockito.mock(McaApi.class);

        when(mcaApi.getOriginalPartContent(Matchers.<String>any(), Matchers.<AnalysisPart>any())).thenReturn(EXISTING_OID_CONTENT.getBytes(StandardCharsets.UTF_8));
        when(mcaApi.analyze(Matchers.<Analysis>any())).thenReturn(analysis);
    }

    private void mockAnalysisPartDao() {
        analysisPartDao = Mockito.mock(AnalysisPartDaoImpl.class);
        when(analysisPartDao.getByOid(NOT_EXISTING_OID)).thenReturn(null);
    }

    private void mockAnalysisDao() throws UnexpectedProcessingException, NotLoadedException, ProcessingNotSavedException {
        analysisDao = Mockito.mock(AnalysisDaoImpl.class);
        when(analysisDao.getByParentAnalysisPart(analysisPart))
                .thenReturn(analysis);
        when(analysisDao.getObjectDAOImpl(analysis)).thenReturn((HandledObjectFile) analysis.getObject());
        when(analysisDao.merge(Matchers.<Analysis>any()))
                .thenAnswer(new Answer<Analysis>() {
                    @Override
                    public Analysis answer(InvocationOnMock invocation) {
                        Object[] args = invocation.getArguments();
                        Analysis analysis = (Analysis) args[0];
                        analysis.setId(ANALYSIS_ID);
                        return analysis;
                    }
                });
        when(analysisDao.create(Matchers.<Analysis>any())).thenReturn(analysis);
    }

    private void mockApplicationPreferenceManager() {

        PowerMockito.mockStatic(Lifecycle.class);
        PowerMockito.mockStatic(Contexts.class);

        when(Contexts.isEventContextActive()).thenReturn(true);
        when(Contexts.isApplicationContextActive()).thenReturn(true);

        applicationPreferenceManager = Mockito.mock(ApplicationPreferenceManager.class);
        when(applicationPreferenceManager.getStringValue("root_oid"))
                .thenReturn("1.3.6.1.4.1.12559.11.1.2.1.4.");
        when(applicationPreferenceManager.getStringValue("object_for_validator_detector_repository"))
                .thenReturn("/opt/EVSClient_prod/validatedObjects/validatorDetector");
        when(applicationPreferenceManager.getStringValue("application_url"))
                .thenReturn("https://gazelle.ihe.net/EU-CAT/");
        when(applicationPreferenceManager.getApplicationUrl()).thenReturn("http://localhost:8380/EVSClient/");
        metadataServiceProvider = Mockito.mock(MetadataServiceProvider.class);
        Service service = new ServiceBuilder()
                .setName("gazelle-mca")
                .setVersion("mcaVersion")
                .build();
        when(metadataServiceProvider.getMetadata()).thenReturn(service);
    }

    private void mockAnalysisManager() throws UnauthorizedException, NotFoundException, ForbiddenAccessException {
        doReturn(analysis).when(analysisManagerSpied).getObjectByOID(EXISTING_OID_CONTENT, null, gazelleIdentity);
        doReturn(new Analysis(new HandledObject(EXISTING_ID_CONTENT.getBytes(), null), null))
              .when(analysisManagerSpied).getObjectByID(1, null, gazelleIdentity);
        doReturn(analysis).when(analysisManagerSpied).getObjectByOID(EXISTING_OID, null, gazelleIdentity);
    }

    private void mockFacesMessages(String key, String value) {
        PowerMockito.mockStatic(Component.class);
        PowerMockito.mockStatic(FacesContext.class);

        FacesMessages facesMessageMock = PowerMockito.mock(FacesMessages.class);
        PowerMockito.when(FacesMessages.instance()).thenReturn(facesMessageMock);

        FacesContext facesContextMock = PowerMockito.mock(FacesContext.class);
        ExternalContext externalContextMock = PowerMockito.mock(ExternalContext.class);
        PowerMockito.when(externalContextMock.getRequestParameterMap()).thenReturn(getForgedRequestParameterMap(key, value));
        PowerMockito.when(facesContextMock.getExternalContext()).thenReturn(externalContextMock);
        PowerMockito.when(FacesContext.getCurrentInstance()).thenReturn(facesContextMock);
    }

    private Map<String, String> getForgedRequestParameterMap(String key, String value) {
        Map<String, String> forgedMap = new HashMap<>();
        forgedMap.put(key, value);
        return forgedMap;
    }

    @Test
    public void initWithOidTestExistingOid() throws UnauthorizedException, NotFoundException, ForbiddenAccessException {
        mockFacesMessages(QueryParamMca.PROCESSING_OID, EXISTING_OID_CONTENT);
        analysisBeanGui.initFromUrl();
        Analysis analysis = analysisBeanGui.getSelectedObject();
        assertEquals(EXISTING_OID_CONTENT, new String(analysis.getObject().getContent()));
    }

    @Test(expected = NotFoundException.class)
    public void initWithOidTestNotExistingOid()
          throws UnauthorizedException, NotFoundException, ForbiddenAccessException {
        mockFacesMessages(QueryParamMca.PROCESSING_OID, NOT_EXISTING_OID);
        analysisBeanGui.initFromUrl();
    }

    @Test
    public void initWithIdTestExistingId() throws UnauthorizedException, NotFoundException, ForbiddenAccessException {
        mockFacesMessages(QueryParamMca.PROCESSING_ID, "1");
        analysisBeanGui.initFromUrl();
        Analysis analysis = analysisBeanGui.getSelectedObject();
        assertEquals(EXISTING_ID_CONTENT, new String(analysis.getObject().getContent()));
    }

    @Test(expected = NotFoundException.class)
    public void initWithIdTestNotExistingId() throws UnauthorizedException, NotFoundException,
          ForbiddenAccessException {
        mockFacesMessages(QueryParamMca.PROCESSING_ID, NOT_EXISTING_ID);
        analysisBeanGui.initFromUrl();
    }

    @Test(expected=NumberFormatException.class)
    public void initWithIdTestNotValidId() throws UnauthorizedException, NotFoundException, ForbiddenAccessException {
        mockFacesMessages(QueryParamMca.PROCESSING_ID, "notValidId");
        analysisBeanGui.initFromUrl();
    }


    @Test
    public void executeWithString() {

        final File f = loadFile("/contentanalyzer/validXmlFile.xml");
        String fileContent = "";
        try {
            fileContent = new String(FileUtils.readFileToByteArray(f));
        } catch (IOException e) {
            fail("Unable to read file.");
        }
        try {
            analyzerManager.executeWithString(fileContent, AnalyzerManager.EXECUTE_FROM_STR_DESCRIPTION, null, null);
        } catch (UnexpectedProcessingException | DecodedPartNotSavedException | ProcessingNotSavedException | NotLoadedException e) {
            fail("No exception is supposed to be raised");
        }
        assertNotNull(analysis);
    }

    @Ignore
    @Test
    public void executeWithStringTest() throws UnauthorizedException, NotFoundException, ForbiddenAccessException {
        //analysisBeanGui.setMessagePart();
        mockFacesMessages(QueryParamMca.PROCESSING_OID, EXISTING_OID);
        analysisBeanGui.initFromUrl();
        analysisBeanGui.setDocumentText("<s:Body xmlns:s=\"http://www.w3.org/2003/05/soap-envelope\">\n" +
                "        <wsnt:Subscribe xmlns:wsnt=\"http://docs.oasis-open.org/wsn/b-2\">\n" +
                "            <wsnt:ConsumerReference>\n" +
                "                <a:Address xmlns:a=\"http://www.w3.org/2005/08/addressing\">http://abc.com/1123</a:Address>\n" +
                "            </wsnt:ConsumerReference>\n" +
                "            <wsnt:Filter>\n" +
                "                <wsnt:TopicExpression Dialect=\"http://docs.oasis-open.org/wsn/t-1080 1/TopicExpression/Simple\">\n" +
                "                    ihe:FullDocumentEntry\n" +
                "                </wsnt:TopicExpression>\n" +
                "                <rim:AdhocQuery xmlns:rim=\"urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0\" id=\"urn:uuid:aa2332d0-f8fe-11e0-be50-0800200c9a66\">\n" +
                "                    <rim:Slot name=\"$XDSDocumentEntryPatientId\">\n" +
                "                        <rim:ValueList>\n" +
                "                            <rim:Value>'1234'</rim:Value>\n" +
                "                        </rim:ValueList>\n" +
                "                    </rim:Slot>\n" +
                "                </rim:AdhocQuery>\n" +
                "            </wsnt:Filter>\n" +
                "            <wsnt:InitialTerminationTime>2014-05-19T16:16:02.7686801+01:00</wsnt:InitialTerminationTime>\n" +
                "        </wsnt:Subscribe>\n" +
                "    </s:Body>");
        analysisBeanGui.executeOnEditedPart();
        /*analysisBeanGui.setOriginalMessagePart("<s:Body xmlns:s=\"http://www.w3.org/2003/05/soap-envelope\">\n" +
                "        <wsnt:Subscribe xmlns:wsnt=\"http://docs.oasis-open.org/wsn/b-2\">\n" +
                "            <wsnt:ConsumerReference>\n" +
                "                <a:Address xmlns:a=\"http://www.w3.org/2005/08/addressing\">http://abc.com/1123</a:Address>\n" +
                "            </wsnt:ConsumerReference>\n" +
                "            <wsnt:Filter>\n" +
                "                <wsnt:TopicExpression Dialect=\"http://docs.oasis-open.org/wsn/t-1080 1/TopicExpression/Simple\">\n" +
                "                    ihe:FullDocumentEntry\n" +
                "                </wsnt:TopicExpression>\n" +
                "                <rim:AdhocQuery xmlns:rim=\"urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0\" id=\"urn:uuid:aa2332d0-f8fe-11e0-be50-0800200c9a66\">\n" +
                "                    <rim:Slot name=\"$XDSDocumentEntryPatientId\">\n" +
                "                        <rim:ValueList>\n" +
                "                            <rim:Value>'1234'</rim:Value>\n" +
                "                        </rim:ValueList>\n" +
                "                    </rim:Slot>\n" +
                "                </rim:AdhocQuery>\n" +
                "            </wsnt:Filter>\n" +
                "            <wsnt:InitialTerminationTime>2014-05-19T16:16:02.7686801+01:00</wsnt:InitialTerminationTime>\n" +
                "        </wsnt:Subscribe>\n" +
                "    </s:Body>");
        analysisBeanGui.setDisplayedMessagePart();*/
        analysisBeanGui.executeOnEditedPart();

        analysis = analysisBeanGui.getAnalysis();

        assertEquals("XML", analysis.getRootAnalysisPart().getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals("SOAP Body", analysis.getRootAnalysisPart().getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getDocType());
        assertEquals("DSUB", analysis.getRootAnalysisPart().getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getDocType());
    }

    @Test
    public void test_reset() {
        analysisBeanGui.reset();
        assertNull(analysisBeanGui.getSelectedObject());
        assertNull(analysisBeanGui.getUploadedFileContent());
        assertArrayEquals(new byte[]{}, analysisBeanGui.getMessagePart());
    }



    @Test
    public void getValidatorUrlTest() throws UnauthorizedException, NotFoundException, ForbiddenAccessException {

        analysis.setOid(EXISTING_OID);
        mockFacesMessages(QueryParamMca.PROCESSING_OID, EXISTING_OID);
        analysisBeanGui.initFromUrl();
        String validationLink = analysisBeanGui.revalidate();
        assertEquals("messageContentAnalyzer.seam?oid=" + EXISTING_OID, validationLink);
    }

    @Test
    public void performAnotherValidationTest() {
        assertEquals("messageContentAnalyzer.seam", analysisBeanGui.performAnotherValidation());
    }

    @Test
    public void getValidationPermanentLinkFromDbNotValidMcaOid() {
        assertEquals("not performed", analysisBeanGui.getValidationPermanentLinkFromDb(""));
    }

    @Test
    //FIXME need a valid oid
    public void getValidationPermanentLinkFromDbValidMcaOid() {
        assertEquals("not performed", analysisBeanGui.getValidationPermanentLinkFromDb(""));
    }

    @Test
    public void executeFromPath() throws DecodedPartNotSavedException, UnexpectedProcessingException, ProcessingNotSavedException, NotLoadedException {

        final File f = loadFile("/contentanalyzer/validXmlFile.xml");
        Analysis analysis = null;
        try {
            analysis = analyzerManager.executeFromPath(f.getPath(), f.getName(), null, null);
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised");
        }
        assertNotNull(analysis);
    }

    @Test(expected = UnexpectedAnalysisException.class)
    public void executeFromNotValidPath() throws UnexpectedProcessingException, DecodedPartNotSavedException, ProcessingNotSavedException, NotLoadedException {
        analyzerManager.executeFromPath("notValidPath", "noName", null, null);
    }

    @Test
    public void executeTest() throws IOException, DecodedPartNotSavedException, UnexpectedProcessingException, ProcessingNotSavedException, NotLoadedException {

        OtherCallerMetadata otherCallerMetadata = (OtherCallerMetadata) new CallerMetadataFactory().getCallerMetadata("toolOid",
                "externalId", "proxyType", null);
        final File f = loadFile("/contentanalyzer/validXmlFile.xml");
        HandledObject object = new HandledObject(FileUtils.readFileToByteArray(f), f.getName());

        Analysis analysisToReturn =  analysisManager.create(object, otherCallerMetadata, null);;
        Analysis analysis = null;

        when(analysisDao.create(Matchers.<Analysis>any())).thenReturn(analysisToReturn);
        when(mcaApi.analyze(Matchers.<Analysis>any())).thenReturn(analysisToReturn);

        try {
            analysis = analyzerManager.execute(FileUtils.readFileToByteArray(f), f.getName(), otherCallerMetadata, null);
        } catch (UnexpectedAnalysisException e) {
            fail("No exception should be thrown here");
        }
        assertNotNull(analysis);
        ProxyCallerMetadata caller = (ProxyCallerMetadata) analysis.getCaller();
        assertEquals(EXISTING_OID, analysis.getOid());
        assertEquals("toolOid", caller.getToolOid());
        assertEquals("proxyType", caller.getProxyType());
        assertEquals("externalId", caller.getToolObjectId());
    }

    @Test
    public void isZipNodeTest() {
        AnalysisPart node = new AnalysisPart();
        node.setDocType("ZIP");
        assertTrue(analyzerManager.isZipNode(node));
        node.setDocType("XDM ZIP");
        assertTrue(analyzerManager.isZipNode(node));
        node.setDocType("TEST");
        assertFalse(analyzerManager.isZipNode(node));
    }



    @Test
    public void XmlFileWithUTF8Bom() {

        final File f = loadFile("/contentanalyzer/HPRIM_BOM.xml");
        byte[] fileContent = new byte[]{};
        try {
            fileContent = FileUtils.readFileToByteArray(f);
        } catch (IOException e) {
            fail("Unable to read file.");
        }
        try {
            HandledObject object = new HandledObject(new ContentConverter().protect(fileContent), "test");
            analysis = analysisManager.create(object, null, null);
            // Start analyze
            analysis = new McaApi(new AnalysisManager(analysisDao, applicationPreferenceManager, oidGeneratorManager,
                    metadataServiceProvider, processingCacheManager),m,null,null).analyze(analysis);
            analysis.setAnalysisStatus(AnalysisStatus.DONE);
        } catch (UnexpectedProcessingException e) {
            fail("No exception is supposed to be raised");
        }
        assertNotNull(analysis);
        assertEquals("XML", analysis.getRootAnalysisPart().getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysis.getRootAnalysisPart().getChildPart().get(0).getChildPart().get(0).getEncodedType());
    }




}