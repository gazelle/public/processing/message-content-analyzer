package net.ihe.gazelle.mca.contentanalyzer.adapters.analysis.gui;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.interfaces.ForbiddenAccessException;
import net.ihe.gazelle.evsclient.application.interfaces.NotFoundException;
import net.ihe.gazelle.evsclient.application.interfaces.UnauthorizedException;
import net.ihe.gazelle.evsclient.domain.processing.EVSCallerMetadata;
import net.ihe.gazelle.evsclient.domain.processing.EntryPoint;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.interlay.dao.HandledObjectFile;
import net.ihe.gazelle.evsclient.interlay.servlet.Upload;
import net.ihe.gazelle.mca.contentanalyzer.adapters.config.QueryParamMca;
import net.ihe.gazelle.mca.contentanalyzer.application.McaApi;
import net.ihe.gazelle.mca.contentanalyzer.application.analysis.AnalysisManager;
import net.ihe.gazelle.mca.contentanalyzer.application.analysis.AnalysisPartManager;
import net.ihe.gazelle.mca.contentanalyzer.application.config.McaConfigManager;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import net.ihe.gazelle.mca.contentanalyzer.business.model.Analysis;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;
import net.ihe.gazelle.mca.contentanalyzer.business.model.EncodedType;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.McaConfigDaoStub;
import org.apache.commons.io.FileUtils;
import org.jboss.seam.Component;
import org.jboss.seam.faces.FacesMessages;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static net.ihe.gazelle.files.FilesUtils.loadFile;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({FacesMessages.class, Component.class, FacesContext.class, ExternalContext.class, Upload.class})
public class AnalysisBeanGuiTest {

   public static final String EXISTING_OID_CONTENT = "existingOID";
   private static final String FILEPATH = "filepath";

   @Mock
   AnalysisManager analysisManager;
   @Mock
   AnalysisPartManager analysisPartManager;

   @Mock
   McaApi mcaApi;

   @Mock
   ApplicationPreferenceManager applicationPreferenceManager;

   AnalysisBeanGui analysisBeanGui;
   Analysis analysis;

   @Before
   public void setUp()
         throws UnauthorizedException, NotFoundException, ForbiddenAccessException, UnexpectedAnalysisException {
      MockitoAnnotations.initMocks(this);

      staticMockFacesMessages(QueryParamMca.PROCESSING_OID, EXISTING_OID_CONTENT);
      staticMockUpload();

      analysis = getDummyAnalysis();

      when(applicationPreferenceManager.getStringValue("mca_tool_oid")).thenReturn("1.2.3.4.5.6");

      when(mcaApi.getOriginalPartContent(Matchers.<String>any(), Matchers.<AnalysisPart>any())).thenReturn(
            EXISTING_OID_CONTENT.getBytes(StandardCharsets.UTF_8));
      when(mcaApi.analyze(Matchers.<Analysis>any())).thenReturn(analysis);

      when(analysisManager.getObjectByOID(EXISTING_OID_CONTENT, null, null)).thenReturn(analysis);
      when(analysisManager.getAnalyzedObjectFilePath(analysis)).thenReturn(FILEPATH);

      analysisBeanGui = new AnalysisBeanGui(analysisManager, analysisPartManager,
            new McaConfigManager(new McaConfigDaoStub()), mcaApi, null,
            null, null, null, applicationPreferenceManager, null);
   }

   @Test
   public void validate() {
      staticMockFacesMessages(null, null);
      String validationLink = analysisBeanGui.validate("objectPath", ValidationType.XML, 0, 0, "oid");
      assertEquals("/validate.seam?startOffset=0&endOffset=0&key=keyFilePath&validationType=XML&toolOid=1.2.3.4.5.6&externalId=oid",
            validationLink);
   }

   @Test
   public void validateWithDecodedPartPathTest() {

      final File f = loadFile("/contentanalyzer/xmlNamespaces.xml");

      final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
      analysisPart.setDocType("DOCUMENT");
      analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
      analysisPart.setStartOffset(0);
      analysisPart.setOid("12");
      analysisPart.setDecodedPartFilePath("/contentanalyzer/xmlNamespaces.xml");
      try {
         analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
      } catch (IOException e) {
         System.out.println(e.getMessage());
      }

      assertEquals("/validate.seam?startOffset=0&endOffset=1594&key=keyFilePath&toolOid=1.2.3.4.5.6&externalId=12",
            analysisBeanGui.validate("/contentanalyzer/xmlNamespaces.xml", analysisPart));
   }

   @Test
   public void validateDocumentTest() {

      final File f = loadFile("/contentanalyzer/xmlNamespaces.xml");

      final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
      analysisPart.setDocType("DOCUMENT");
      analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
      analysisPart.setStartOffset(0);
      analysisPart.setOid("12");
      try {
         analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
      } catch (IOException e) {
         System.out.println(e.getMessage());
      }

      assertEquals("/validate.seam?startOffset=0&endOffset=1594&key=keyFilePath&toolOid=1.2.3.4.5.6&externalId=12",
            analysisBeanGui.validate("/contentanalyzer/xmlNamespaces.xml", analysisPart));
   }

   private Analysis getDummyAnalysis() {
      AnalysisPart analysisPart;
      Analysis analysis;

      HandledObjectFile object = new HandledObjectFile(EXISTING_OID_CONTENT.getBytes(), null);
      object.setFilePath(FILEPATH);

      analysis = new Analysis(object,
            new EVSCallerMetadata(EntryPoint.GUI));
      AnalysisPart analysisPartRoot = new AnalysisPart();
      analysisPartRoot.setDocType("txt");
      analysisPart = new AnalysisPart(analysisPartRoot);
      analysisPart.setAnalysis(analysis);
      analysisPart.setDocType("txt");
      analysisPartRoot.setChildPart(Arrays.asList(analysisPart));
      analysis.setRootAnalysisPart(analysisPartRoot);
      return analysis;
   }

   private void staticMockFacesMessages(String key, String value) {
      PowerMockito.mockStatic(Component.class);
      PowerMockito.mockStatic(FacesContext.class);

      FacesMessages facesMessageMock = PowerMockito.mock(FacesMessages.class);
      PowerMockito.when(FacesMessages.instance()).thenReturn(facesMessageMock);

      FacesContext facesContextMock = PowerMockito.mock(FacesContext.class);
      ExternalContext externalContextMock = PowerMockito.mock(ExternalContext.class);
      PowerMockito.when(externalContextMock.getRequestParameterMap())
            .thenReturn(getForgedRequestParameterMap(key, value));
      PowerMockito.when(facesContextMock.getExternalContext()).thenReturn(externalContextMock);
      PowerMockito.when(FacesContext.getCurrentInstance()).thenReturn(facesContextMock);
   }

   private void staticMockUpload() {
      PowerMockito.mockStatic(Upload.class);
      when(Upload.putFile(Matchers.<String>any())).thenReturn("keyFilePath");
   }

   private Map<String, String> getForgedRequestParameterMap(String key, String value) {
      Map<String, String> forgedMap = new HashMap<>();
      forgedMap.put(key, value);
      return forgedMap;
   }

}
