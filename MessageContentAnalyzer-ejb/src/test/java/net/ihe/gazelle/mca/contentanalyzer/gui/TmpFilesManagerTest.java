package net.ihe.gazelle.mca.contentanalyzer.gui;

import net.ihe.gazelle.mca.contentanalyzer.application.files.TmpFilesManager;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

public class TmpFilesManagerTest {

    TmpFilesManager tmpFilesManager;

    @Before
    public void setUp() {
        tmpFilesManager = new TmpFilesManager();
    }

    @Test
    public void createAndDeleteTest() {

        byte[] testBytesFileContent = new byte[]{ 1, 2, 3, 4};

        File testFile = null;
        try {
            testFile = tmpFilesManager.createTmpFileFromByte("testFile", testBytesFileContent);
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be called there");
        }
        if (testFile != null) {
            assertTrue(testFile.exists());
        } else {
            fail("Temp file was not created as expected");
        }

        tmpFilesManager.deleteTmpFile(testFile);
        assertFalse(testFile.exists());
    }
}
