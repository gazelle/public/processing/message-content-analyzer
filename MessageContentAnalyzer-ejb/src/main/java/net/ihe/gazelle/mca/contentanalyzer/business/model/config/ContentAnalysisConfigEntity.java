package net.ihe.gazelle.mca.contentanalyzer.business.model.config;

import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.ContentAnalysisConfigInterface;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "bytePattern",
        "stringPattern",
        "unwantedContent",
        "startsWith",
        "sample"
})
@XmlRootElement(name = "contentAnalysisConfig")
@Entity
@Table(name = "mca_content_analysis_config", schema = "public")
@SequenceGenerator(name = "content_analysis_config_sequence", sequenceName = "mca_content_analysis_config_seq", allocationSize = 1)
public class ContentAnalysisConfigEntity extends Config implements ContentAnalysisConfigInterface {

    @XmlTransient
    @Id
    @NotNull
    @GeneratedValue(generator = "content_analysis_config_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @XmlElement(type = String.class)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name="mca_starts_with", joinColumns=@JoinColumn(name="content_analysis_config_id"))
    private List<String> startsWith;

    @XmlElement(required = true)
    @Column(name = "byte_pattern")
    private byte[] bytePattern;

    @XmlElement(required = true)
    @Column(name = "string_pattern")
    private String stringPattern;

    @XmlElement(required = true)
    @Column(name = "unwanted_content")
    private String unwantedContent;

    @XmlElement(required = true)
    @Lob
    @Type(type = "text")
    @Column(name = "sample")
    private String sample;

    @XmlTransient
    @Transient
    private static final ConfigType configType = ConfigType.CONTENT_ANALYSIS_CONFIGURATION;

    ///////////////////////////////////////////////////////////////////////////////
    ////////////////////       Constructor       //////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    public ContentAnalysisConfigEntity(final String docType, final ValidationType validationType, final List<String> startsWith,
                                       final byte[] bytePattern, final String stringPattern, final String unwantedContent) {
        super.setDocType(docType);
        super.setValidationType(validationType);
        this.startsWith = startsWith;
        if (bytePattern != null) {
            this.bytePattern = bytePattern.clone();
        } else {
            this.bytePattern = null;
        }
        this.stringPattern = stringPattern;
        this.unwantedContent = unwantedContent;
    }

    public ContentAnalysisConfigEntity() {

    }

    ///////////////////////////////////////////////////////////////////////////////
    ////////////////////    Getter and setter     /////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    @Override
    public List<String> getStartsWith() {
        return this.startsWith;
    }

    @Override
    public void setStartsWith(List<String> startsWith) {
        this.startsWith = startsWith;
    }

    @Override
    public byte[] getBytePattern() {
        if (this.bytePattern != null) {
            return this.bytePattern.clone();
        } else {
            return null;
        }
    }

    @Override
    public void setBytePattern(byte[] bytePattern) {
        if (bytePattern != null) {
            this.bytePattern = bytePattern.clone();
        } else {
            this.bytePattern = null;
        }
    }

    @Override
    public String getStringPattern() {
        return this.stringPattern;
    }

    public void setStringPattern(String stringPattern) {
        if (!stringPattern.isEmpty()) {
            this.stringPattern = stringPattern;
        } else {
            this.stringPattern = null;
        }
    }

    @Override
    public String getUnwantedContent() {
        return this.unwantedContent;
    }

    public void setUnwantedContent(String unwantedContent) {
        if (!unwantedContent.isEmpty()) {
            this.unwantedContent = unwantedContent;
        } else {
            this.unwantedContent = null;
        }
    }

    @Override
    public String getSample() {
        return this.sample;
    }

    public void setSample(String sample) {
        this.sample = sample;
    }

    @Override
    public boolean isHardCoded() {
        return false;
    }

    @Override
    public ConfigType getConfigType() {
        return configType;
    }

    @Override
    public String getStartsWithAsString() {
        if (this.startsWith != null && !this.startsWith.isEmpty()) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(startsWith.get(0));
            stringBuilder.append(" ");
            for (int i = 1 ; i < startsWith.size() ; i++ ) {
                stringBuilder.append(startsWith.get(i));
                stringBuilder.append(" ");
            }
            return stringBuilder.toString();
        }
        return null;
    }

    public boolean match( final List<String> startsWith,
                          final byte[] bytePattern, final String stringPattern, final String unwantedContent) {

        if ((stringPattern != null && getStringPattern() != null && !stringPattern.equals(getStringPattern())) ||
                ((stringPattern == null) != (getStringPattern() == null))) {
            return false;
        }
        if (unwantedContent != null && getUnwantedContent() != null && !unwantedContent.equals(getUnwantedContent()) ||
                ((unwantedContent == null) != (getUnwantedContent() == null))) {
            return false;
        }

        if (getStartsWith() == null) {
            if (startsWith != null) {
                return false;
            }
        } else if (getStartsWith().isEmpty()) {
           return ((startsWith == null) || (startsWith.isEmpty()));
        } else {
            if (startsWith == null || startsWith.size() != getStartsWith().size()) {
                return false;
            }
            Collections.sort(startsWith);
            Collections.sort(getStartsWith());
            if (!startsWith.equals(getStartsWith())) {
                return false;
            }
        }

        if (bytePattern != null && getBytePattern() != null) {
            return Arrays.equals(bytePattern, getBytePattern());
        }
        return (bytePattern == null && getBytePattern() == null);
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        sb.append(this.getDocType());
        sb.append(", ");
        sb.append(this.getValidationType());
        sb.append(", ");
        sb.append(Arrays.toString(this.getBytePattern()));
        sb.append(", ");
        sb.append(this.getStringPattern());
        sb.append(", ");
        sb.append(this.getUnwantedContent());
        if (getStartsWith() != null && !getStartsWith().isEmpty()) {
            sb.append(", {");
            for (int i = 0 ; i < getStartsWith().size()-1 ; i++) {
                sb.append(getStartsWith().get(i));
                sb.append(", ");
            }
            sb.append(getStartsWith().get(getStartsWith().size()-1));
            sb.append("}");
        }
        return sb.toString();
    }
}
