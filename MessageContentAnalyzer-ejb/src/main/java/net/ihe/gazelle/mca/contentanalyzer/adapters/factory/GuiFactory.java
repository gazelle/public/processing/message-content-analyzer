package net.ihe.gazelle.mca.contentanalyzer.adapters.factory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.interlay.factory.EvsCommonApplicationFactory;
import net.ihe.gazelle.mca.contentanalyzer.adapters.analysis.gui.AnalysisBeanGui;
import net.ihe.gazelle.mca.contentanalyzer.adapters.config.gui.ConfigBeanGui;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;

@Name("mcaGuiFactory")
@Scope(ScopeType.APPLICATION)
@AutoCreate
public class GuiFactory {

    @In(value = "mcaApplicationFactory")
    private ApplicationFactory applicationFactory;

    @In(value ="evsCommonApplicationFactory")
    private EvsCommonApplicationFactory evsCommonApplicationFactory;

    @In(value = "applicationPreferenceManager", create = true)
    private ApplicationPreferenceManager applicationPreferenceManager;

    @In(value = "org.jboss.seam.security.identity", create = true)
    private GazelleIdentity gazelleIdentity;

    @Factory(value= "configBeanGui", scope = ScopeType.PAGE)
    public ConfigBeanGui getConfigBeanGui() {
        return new ConfigBeanGui(applicationFactory.getMcaConfigManager(), applicationFactory.getFilesDownloaderManager());
    }

    @Factory(value= "analysisBeanGui", scope = ScopeType.PAGE)
    public AnalysisBeanGui getAnalysisBeanGui() {
        return new AnalysisBeanGui(applicationFactory.getAnalysisManager(), applicationFactory.getAnalysisPartManager(), applicationFactory.getMcaConfigManager(),
                applicationFactory.getMcaApi(), applicationFactory.getAnalyzerManager(), evsCommonApplicationFactory.getCallerMetadataFactory(),
                applicationFactory.getBase64Converter(), applicationFactory.getFilesDownloaderManager(), applicationPreferenceManager,
                gazelleIdentity);
    }

}
