package net.ihe.gazelle.mca.contentanalyzer.application.analyzers.xml;

import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.interlay.gui.document.XmlFormatAdapter;
import net.ihe.gazelle.mca.contentanalyzer.application.analyzers.Analyzer;
import net.ihe.gazelle.mca.contentanalyzer.application.analyzers.MessageContentAnalyzer;
import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.McaConfigDao;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class XmlAnalyzer implements Analyzer {
    private static final long serialVersionUID = -8068911570835253148L;

    private static final Logger LOGGER = LoggerFactory.getLogger(XmlAnalyzer.class);

    public static final String XML_DOCTYPE = "XML";
    private static final ValidationType XML_VALIDATION_TYPE = ValidationType.XML;
    private static final String PART_LOG_TYPE = "XML analysis";
    private static final String XML_FORM_ANALYSIS_AMP_LOG = "XML Form Analysis";

    private final McaConfigDao mcaConfigDao;

    public XmlAnalyzer(McaConfigDao mcaConfigDao) {
        super();
        this.mcaConfigDao = mcaConfigDao;
    }

    @Override
    public AnalysisPart analyze(final MessageContentAnalyzer mca, final AnalysisPart parent)
            throws UnexpectedAnalysisException {
        AnalysisPart child = new AnalysisPart(parent);
        return analyzeXml(mca, child, parent);
    }

    public AnalysisPart analyzeXml(final MessageContentAnalyzer mca, final AnalysisPart child,
                                   final AnalysisPart parent)
            throws UnexpectedAnalysisException {

        boolean isXmlWellFormed = isXmlWellFormed(mca.getMessageByteContent(), 0,
                    new String(mca.getMessageByteContent(), StandardCharsets.UTF_8).length(), parent);

        if (isXmlWellFormed) {
            LOGGER.info("The File is an XML File");
            parent.addLog(PART_LOG_TYPE,"Detected well formed XML, start analysis of XML sub-part");
            child.addLog(PART_LOG_TYPE,"Start analysis as XML sub-part");
            setChildAsXml(mca, child, parent);
            new TagDetector(this.mcaConfigDao,mca)
                    .detectTags(new String(mca.getMessageByteContent(), StandardCharsets.UTF_8), child);
            return child;
        }
        parent.addLog(PART_LOG_TYPE, "End of Analysis : no well formed XML content detected");
        return null;
    }

    private void setChildAsXml(final MessageContentAnalyzer mca, final AnalysisPart child,
                               final AnalysisPart parent) {

        mca.setChildOffsetsAndDecodedPart(child, parent);
        child.setDocType(XML_DOCTYPE);
        child.setValidationType(XML_VALIDATION_TYPE);
        parent.getChildPart().add(child);

    }

    private boolean isXmlWellFormed(byte[] messageBytesContent, final int startOffset, final int endOffset,
                                    final AnalysisPart parent)
            throws UnexpectedAnalysisException {
        boolean result = false;
        try {
            String fileContent = new String(messageBytesContent, StandardCharsets.UTF_8).substring(startOffset, endOffset);
            result = XmlFormatAdapter.assertWellFormed(fileContent);
        } catch (final IOException e) {
            LOGGER.error("Exception during XML analysis from message content ", e);
            throw new UnexpectedAnalysisException("Exception during XML analysis from message content.", e);
        } catch (final ParserConfigurationException e) {
            LOGGER.error("Exception on parser configuration ", e);
            throw new UnexpectedAnalysisException("Exception during XML analysis from message content.", e);
        } catch (final SAXException e) {
            LOGGER.debug("XML not well formed: {}", e.getMessage());
            parent.addLog(XML_FORM_ANALYSIS_AMP_LOG, "XML not well formed: " + e.getMessage());
        }
        return result;
    }
}
