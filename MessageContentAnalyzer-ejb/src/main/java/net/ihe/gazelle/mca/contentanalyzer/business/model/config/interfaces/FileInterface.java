package net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces;

public interface FileInterface {

    String getFileName();

    boolean isRegex();

    String getXValInputType();
}
