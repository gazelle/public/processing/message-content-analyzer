package net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces;

public interface XmlTagConfigInterface extends ConfigInterface {

    String getTag();

    String getNamespace();

    boolean hasSameDetectionElements(XmlTagConfigInterface xmlTagConfigInterface);

    XmlNodeMatcher getMatcher();
}
