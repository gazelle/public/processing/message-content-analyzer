package net.ihe.gazelle.mca.contentanalyzer.application.analyzers;

import net.ihe.gazelle.evsclient.interlay.gui.document.ContentConverter;
import net.ihe.gazelle.mca.contentanalyzer.application.utils.DocTypeUtils;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;
import net.ihe.gazelle.mca.contentanalyzer.business.model.EncodedType;

import java.io.Serializable;

public class MessageContentAnalyzer implements Serializable {
    private static final long serialVersionUID = -333802749228093354L;

    private static final String PART_LOG_TYPE = "Message Content Analyzer";

    private byte[] messageByteContent;

    private byte[] messageName;

    private final Analyzer xmlAnalyzer;

    private final Analyzer contentAnalysisTypeDetector;

    private final Analyzer mimeTypeDetector;

    private final Analyzer b64Analyzer;

    public MessageContentAnalyzer(Analyzer xmlAnalyzer, Analyzer contentAnalysisTypeDetector,
                                  Analyzer mimeTypeDetector, Analyzer b64Analyzer) {
        this(xmlAnalyzer, contentAnalysisTypeDetector, mimeTypeDetector, b64Analyzer, null);
    }

    public boolean isZipDocType(String docType) {
        return ((MimeTypeDetector)mimeTypeDetector).getZipStructureWithDocType(docType)!=null;
    }

    public MessageContentAnalyzer(Analyzer xmlAnalyzer, Analyzer contentAnalysisTypeDetector,
                                  Analyzer mimeTypeDetector, Analyzer b64Analyzer, byte[] messageName) {
        this.xmlAnalyzer = xmlAnalyzer;
        this.contentAnalysisTypeDetector = contentAnalysisTypeDetector;
        this.mimeTypeDetector = mimeTypeDetector;
        this.b64Analyzer = b64Analyzer;
        if (messageName != null) {
            this.messageName = messageName.clone();
        } else {
            this.messageName = null;
        }
    }

    public byte[] getMessageByteContent() {
        if (messageByteContent != null) {
            return messageByteContent;
        } else {
            return null;
        }
    }

    public void setMessageByteContent(byte[] messageByteContent) {
        if (messageByteContent != null) {
            this.messageByteContent = messageByteContent;
        } else {
            this.messageByteContent = null;
        }
    }

    public byte[] getMessageName() {
        if (messageName != null) {
            return messageName.clone();
        } else {
            return null;
        }
    }

    public void setMessageName(byte[] messageName) {
        if (messageName != null) {
            this.messageName = messageName.clone();
        } else {
            this.messageName = null;
        }
    }

    public void analyzeMessageContent(byte[] messageContent, AnalysisPart analysisPart)
            throws UnexpectedAnalysisException {

        setMessageByteContent(
                new ContentConverter().protect(messageContent));

        analysisPart.addLog(PART_LOG_TYPE,"Start analysis");

        AnalysisPart child;

        child = mimeTypeDetector==null?null:mimeTypeDetector.analyze(this, analysisPart);
        if (child != null) {
            return;
        }
        child = b64Analyzer==null?null:b64Analyzer.analyze(this, analysisPart);
        if (child != null) {
            return;
        }

        child = contentAnalysisTypeDetector==null?null:contentAnalysisTypeDetector.analyze(this, analysisPart);
        if (child != null) {
            return;
        }

        if (xmlAnalyzer!=null) {
            xmlAnalyzer.analyze(this, analysisPart);
        }
    }

    public void setChildOffsetsAndDecodedPart(AnalysisPart child, AnalysisPart parent) {

        if (DocTypeUtils.isBase64(parent.getDocType())) {
            child.setStartOffset(0);
            child.setEndOffset(getMessageByteContent().length);
            child.setEncodedType(EncodedType.B64_ENCODED);
            child.setDecodedPart(parent.getDecodedPart());
            parent.setDecodedPart(null);
        } else {
            child.setStartOffset(parent.getStartOffset());
            child.setEndOffset(parent.getEndOffset());
            child.setEncodedType(parent.getEncodedType());
        }
    }

}
