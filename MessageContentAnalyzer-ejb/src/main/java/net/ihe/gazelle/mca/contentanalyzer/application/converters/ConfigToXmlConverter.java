package net.ihe.gazelle.mca.contentanalyzer.application.converters;

import net.ihe.gazelle.mca.contentanalyzer.business.model.config.*;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.ConfigInterface;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;


public class ConfigToXmlConverter {

    public String configToXml(List<ConfigInterface> configInterfaceList) throws JAXBException {

        ConfigsFromDB configsFromDB = this.toConfigsFromDB(configInterfaceList);

        JAXBContext jaxbContext = JAXBContext.newInstance(ConfigsFromDB.class, XmlTagConfigEntity.class,
                MimeTypeConfigEntity.class, ContentAnalysisConfigEntity.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        StringWriter sw = new StringWriter();
        jaxbMarshaller.marshal(configsFromDB, sw);
        return sw.toString();
    }

    public ConfigsFromDB xmlToConfig(String xmlString) throws JAXBException {

        JAXBContext jc = JAXBContext.newInstance(ConfigsFromDB.class, XmlTagConfigEntity.class,
                MimeTypeConfigEntity.class, ContentAnalysisConfigEntity.class);
        Unmarshaller u = jc.createUnmarshaller();

        Reader reader = new InputStreamReader(new ByteArrayInputStream
                (xmlString.getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8);
        return (ConfigsFromDB) u.unmarshal(reader);
    }

    private ConfigsFromDB toConfigsFromDB(List<ConfigInterface> configInterfaceList) {

        ConfigsFromDB configsFromDB = new ConfigsFromDB();
        for (ConfigInterface config : configInterfaceList) {
            if (config.getConfigType().equals(ConfigType.TAG_CONFIG)) {
                configsFromDB.getXmlTagConfigEntities().add((XmlTagConfigEntity) config);
            } else if (config.getConfigType().equals(ConfigType.MIME_TYPE_CONFIG)) {
                if (!config.isHardCoded()) {
                    configsFromDB.getMimeTypeConfigEntities().add((MimeTypeConfigEntity) config);
                }
            } else if (config.getConfigType().equals(ConfigType.CONTENT_ANALYSIS_CONFIGURATION)) {
                if (!config.isHardCoded()) {
                    configsFromDB.getContentAnalysisConfigEntities().add((ContentAnalysisConfigEntity) config);
                }
            }
        }

        return configsFromDB;
    }
}
