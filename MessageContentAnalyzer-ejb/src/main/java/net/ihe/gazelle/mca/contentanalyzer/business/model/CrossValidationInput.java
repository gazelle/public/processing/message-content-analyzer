package net.ihe.gazelle.mca.contentanalyzer.business.model;

import net.ihe.gazelle.evsclient.connector.model.EVSClientCrossValidatedObject;

public class CrossValidationInput implements EVSClientCrossValidatedObject {

    private String inputType;
    private byte[] messageContent;
    private String type;

    public CrossValidationInput(String inputType, byte[] messageContent, String type) {
        this.inputType = inputType;
        if (messageContent != null) {
            this.messageContent = messageContent.clone();
        } else {
            this.messageContent = null;
        }
        this.type = type;
    }

    @Override
    public String getInputType() {
        return this.inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

    @Override
    public byte[] getMessageContent() {
        if (this.messageContent != null) {
            return this.messageContent.clone();
        } else {
            return null;
        }
    }

    public void setMessageContent(byte[] messageContent) {
        if (messageContent != null) {
            this.messageContent = messageContent.clone();
        } else {
            this.messageContent = null;
        }
    }

    @Override
    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
