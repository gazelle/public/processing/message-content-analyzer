package net.ihe.gazelle.mca.contentanalyzer.business.model.config;

public enum ConfigType {

    TAG_CONFIG("Tag Configuration"),
    MIME_TYPE_CONFIG("Mime Type Configuration"),
    CONTENT_ANALYSIS_CONFIGURATION("Content Analysis Configuration");

    private final String name;

    ConfigType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
