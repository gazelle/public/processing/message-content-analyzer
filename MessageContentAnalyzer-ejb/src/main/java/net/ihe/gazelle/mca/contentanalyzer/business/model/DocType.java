package net.ihe.gazelle.mca.contentanalyzer.business.model;

public enum DocType {

    XML("XML"),
    DOCUMENT("DOCUMENT"),
    PDF("PDF"),
    ZIP("ZIP"),
    BASE64("BASE64"),
    HL7_V2("HL7v2"),
    CERTIFICATE("Certificate"),
    HTTP("HTTP"),
    MTOM("MTOM"),
    SYSLOG("SYSLOG"),
    FILE_FOLDER_ARCHIVE("Files/Folder/Archive"),
    DICOM("DICOM");

    private String value;

    DocType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
