package net.ihe.gazelle.mca.contentanalyzer.application.files;

import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.mca.contentanalyzer.application.analysis.AnalysisPartManager;
import net.ihe.gazelle.mca.contentanalyzer.application.analyzers.MimeTypeDetector;
import net.ihe.gazelle.mca.contentanalyzer.application.converters.DicomToTxtConverter;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import net.ihe.gazelle.mca.contentanalyzer.business.model.Analysis;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;
import net.ihe.gazelle.mca.contentanalyzer.business.model.DocType;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.ContentAnalysisConfigEnum;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.tika.io.IOUtils;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;

public class FilesDownloaderManager implements Serializable {
    private static final long serialVersionUID = 6691279722238528323L;

    private static final String TEXT_PLAIN_CONTENT_TYPE = "text/plain";
    private static final String ZIP_CONTENT_TYPE = "application/zip";
    private static final String DICOM_CONTENT_TYPE ="application/dicom";
    private static final String XML_CONTENT_TYPE = "text/xml";
    private static final String PDF_CONTENT_TYPE = "application/pdf";
    private static final String HL7_CONTENT_TYPE = "application/hl7-v2";
    private static final String CERTIFICATE_CONTENT_TYPE = "application/x-pem-file";
    private static final String B64_CONTENT_TYPE = "application/base64";

    private static final String DOWNLOADED_XML_FILE_SUFFIX = ".xml";
    private static final String DOWNLOADED_PDF_FILE_SUFFIX = ".pdf";
    private static final String DOWNLOADED_TXT_FILE_SUFFIX = ".txt";
    private static final String DOWNLOADED_HL7_FILE_SUFFIX = ".hl7";
    private static final String DOWNLOADED_SYSLOG_FILE_SUFFIX = ".syslog";
    private static final String DOWNLOADED_CERTIFICATE_FILE_SUFFIX = ".pem";
    private static final String DOWNLOADED_DICOM_FILE_SUFFIX = ".dcm";
    private static final String DOWNLOADED_ZIP_FILE_SUFFIX = ".zip";

    private static final Logger LOGGER = LoggerFactory.getLogger(FilesDownloaderManager.class);
    private static final String CONTENT_DISPOSITION = "Content-Disposition";
    private static final String ATTACHMENT_FILENAME_FORMAT = "attachment; filename=\"{0}\"";

    private final AnalysisPartManager analysisPartManager;

    private final DicomToTxtConverter dicomToTxtConverter;

    public FilesDownloaderManager(AnalysisPartManager analysisPartManager, DicomToTxtConverter dicomToTxtConverter) {
        this.analysisPartManager = analysisPartManager;
        this.dicomToTxtConverter = dicomToTxtConverter;
    }

    public void downloadConfigs(final String xmlConfigs, final FacesContext facesContext, String configDescription) {

        final HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();

        response.setContentType(XML_CONTENT_TYPE);
        response.setHeader(CONTENT_DISPOSITION,
                MessageFormat.format(ATTACHMENT_FILENAME_FORMAT,configDescription));
        final byte[] byteArray = xmlConfigs.getBytes(StandardCharsets.UTF_8);

        addBodyContentToHttpResponse(byteArray, response, facesContext);
    }

    public void downloadFile(final byte[] contentToDownload, final AnalysisPart node, final Analysis analysis,
                             final FacesContext facesContext) {
        final HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();

        response.setContentType(getContentTypeForDownload(node));
        response.setHeader(CONTENT_DISPOSITION,
                MessageFormat.format(ATTACHMENT_FILENAME_FORMAT,
                        getDescriptionForDownload(node, analysis) + getSuffixForDownload(node)));
        addBodyContentToHttpResponse(contentToDownload, response, facesContext);
    }
    public void downloadFile(final String contentToDownload, final AnalysisPart node, final Analysis analysis,
                                    final FacesContext facesContext) {
        downloadFile(contentToDownload.getBytes(StandardCharsets.UTF_8),node,analysis,facesContext);
    }

    public void downloadPdfFile(final byte[] messageBytesContent, final AnalysisPart node,
                                final Analysis analysis,
                                final FacesContext facesContext) {
        try {
            final HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
            response.setContentType(PDF_CONTENT_TYPE);
            response.setHeader(CONTENT_DISPOSITION,
                    MessageFormat.format(ATTACHMENT_FILENAME_FORMAT,
                            getDescriptionForDownload(node, analysis) + DOWNLOADED_PDF_FILE_SUFFIX));
            addBodyContentToHttpResponse(messageBytesContent, response, facesContext);
        } catch (Exception e) {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR,"download failure", e);
        }
    }
    public void downloadDICOMFile(final byte[] messageBytesContent, final AnalysisPart node,
                                   final Analysis analysis,
                                   final FacesContext facesContext) throws UnexpectedAnalysisException {

        final HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();

        response.setContentType(DICOM_CONTENT_TYPE);
        response.setHeader(CONTENT_DISPOSITION,
                MessageFormat.format(ATTACHMENT_FILENAME_FORMAT,
                getDescriptionForDownload(node, analysis) + DOWNLOADED_DICOM_FILE_SUFFIX));
        final byte[] byteContent = dicomToTxtConverter.dicom2txt(messageBytesContent);

        addBodyContentToHttpResponse(byteContent, response, facesContext);
    }

    public void downloadZipFile(final String pathToDownload, final AnalysisPart node, final Analysis analysis,
                                       final FacesContext facesContext) {

        final HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();

        response.setContentType(ZIP_CONTENT_TYPE);
        response.setHeader(CONTENT_DISPOSITION,
                MessageFormat.format(ATTACHMENT_FILENAME_FORMAT,
                getDescriptionForDownload(node, analysis) + DOWNLOADED_ZIP_FILE_SUFFIX));

        byte[] byteContent;
        try {
            byteContent = FileUtils.readFileToByteArray(new File(pathToDownload));
        } catch (IOException e) {
            LOGGER.error("Unable to read file to download : ", e);
            return;
        }

        addBodyContentToHttpResponse(byteContent, response, facesContext);
    }

    private void addBodyContentToHttpResponse(byte[] byteArray, HttpServletResponse response, FacesContext facesContext) {
        try(ServletOutputStream servletOutputStream = response.getOutputStream() ; ByteArrayInputStream input = new ByteArrayInputStream(byteArray)) {
            response.setContentLength(byteArray.length);
            IOUtils.copyLarge(input, servletOutputStream);
            facesContext.responseComplete();
        } catch (final IOException e) {
            LOGGER.error("Failed to download file", e);
            try {
                IOUtils.write("Failed to download file " + ExceptionUtils.getFullStackTrace(e), response.getOutputStream());
            } catch (final IOException e1) {
                LOGGER.error("Failed to send error to browser", e);
            }
        }
    }

    public String getDescriptionForDownload(AnalysisPart node, Analysis analysis) {

        if (node.getDocType().startsWith(MimeTypeDetector.FILE_PREFIX)) {
            int lastSlashIndex = node.getDocType().lastIndexOf('/');
            if (lastSlashIndex > 0) {
                return node.getDocType().substring(lastSlashIndex + 1);
            }
            return node.getDocType().replace(MimeTypeDetector.FILE_PREFIX, "");
        }

        String result = analysis.getObject().getOriginalFileName() + "_" + node.getDocType();
        result = result.replace("/", "_");
        result = result.replace('.', '_');
        result = result.replace(MimeTypeDetector.FOLDER_PREFIX, "");
        return result;
    }

    private String getSuffixForDownload(AnalysisPart node) {

        if (analysisPartManager.isXmlChild(node)) {
            return DOWNLOADED_XML_FILE_SUFFIX;
        } else if (node .getDocType().equals("PDF")) {
            return DOWNLOADED_PDF_FILE_SUFFIX;
        } else if (node .getDocType().equals("HL7v2")) {
            return DOWNLOADED_HL7_FILE_SUFFIX;
        } else if (node .getDocType().equals(ContentAnalysisConfigEnum.SYSLOG.getDocType())) {
            return DOWNLOADED_SYSLOG_FILE_SUFFIX;
        } else if (node .getDocType().equals("Certificate")) {
            return DOWNLOADED_CERTIFICATE_FILE_SUFFIX;
        } else if (node.getDocType().equals("DOCUMENT")) {
            if (node.getChildPart()!= null && !node.getChildPart().isEmpty()) {
                return getSuffixForDownload(node.getChildPart().get(0));
            } else {
                return DOWNLOADED_TXT_FILE_SUFFIX;
            }
        } else if (node.getDocType().startsWith(MimeTypeDetector.FILE_PREFIX)) {
            return "";
        } else {
            return DOWNLOADED_TXT_FILE_SUFFIX;
        }
    }

    private String getContentTypeForDownload(AnalysisPart node) {

        if (analysisPartManager.isXmlChild(node)) {
            return XML_CONTENT_TYPE;
        } else if (node .getDocType().equals("PDF")) {
            return PDF_CONTENT_TYPE;
        } else if (node .getDocType().equals("HL7v2")) {
            return HL7_CONTENT_TYPE;
        } else if (node .getDocType().equals("Certificate")) {
            return CERTIFICATE_CONTENT_TYPE;
        } else if (node.getDocType().equals(DocType.BASE64.getValue())) {
            return B64_CONTENT_TYPE;
        } else if (node.getDocType().equals("DOCUMENT")) {
            if (node.getChildPart()!= null && !node.getChildPart().isEmpty()) {
                return getContentTypeForDownload(node.getChildPart().get(0));
            } else {
                return TEXT_PLAIN_CONTENT_TYPE;
            }
        } else if (node.getDocType().startsWith(MimeTypeDetector.FILE_PREFIX)) {
            String extension = node.getDocType().substring(node.getDocType().lastIndexOf('.'));
            switch (extension) {
                case "xml":
                    return XML_CONTENT_TYPE;
                case "pdf":
                    return PDF_CONTENT_TYPE;
                case "hl7":
                    return HL7_CONTENT_TYPE;
                case "pem":
                    return CERTIFICATE_CONTENT_TYPE;
                default:
                    return TEXT_PLAIN_CONTENT_TYPE;
            }
        } else {
            return TEXT_PLAIN_CONTENT_TYPE;
        }
    }
}
