package net.ihe.gazelle.mca.contentanalyzer.business.model.config;

import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.XmlTagMatcher;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexpMatcher implements XmlTagMatcher {
    private Pattern _regexp;
    private String namespace;

    public RegexpMatcher(String tag, String namespace) {
        _regexp = Pattern.compile(tag, Pattern.DOTALL);
        this.namespace = namespace;
    }

    @Override
    public boolean match(String tag, String namespace) {

        if ((namespace == null && this.namespace == null)) {
            return match(tag);
        } else if (this.namespace != null && namespace != null) {
            return (match(tag) && namespace.equals(this.namespace));
        }
        return false;
    }

    @Override
    public boolean match(String tag) {
        Matcher matcher = _regexp.matcher(tag);
        return matcher.matches();
    }
}
