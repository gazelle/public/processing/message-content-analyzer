package net.ihe.gazelle.mca.contentanalyzer.application.utils;

import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.mca.contentanalyzer.business.model.Analysis;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;
import net.ihe.gazelle.mca.contentanalyzer.business.model.DocType;
import net.ihe.gazelle.mca.contentanalyzer.business.model.EncodedType;

import java.nio.charset.StandardCharsets;
import java.util.List;

public class AnalysisUtils {

    public static String getValidationTypeListAsString(List<ValidationType> validationTypes) {

        String validationTypeListAsString = "";
        if (!validationTypes.isEmpty()){
            for (final ValidationType validationType : validationTypes) {
                if (!validationTypeListAsString.contains(validationType.name())){
                    validationTypeListAsString = validationTypeListAsString.concat(validationType.name()).concat(" ");
                }
            }
        }
        return validationTypeListAsString ;
    }

    public static void addRootAnalysisPart(HandledObject object, Analysis analysis) {
        final AnalysisPart parent = new AnalysisPart(null);
        parent.setDocType(DocType.DOCUMENT.getValue());
        parent.setEncodedType(EncodedType.NOT_ENCODED);
        parent.setStartOffset(0);
        parent.setEndOffset(object.getContent().length);
        analysis.setRootAnalysisPart(parent);
    }

    public static void setOidForAllAnalysisPartChildren(String analysisOid, AnalysisPart rootAnalysisPart) {
        int increment = 0;
        rootAnalysisPart.setOid(analysisOid);
        for (AnalysisPart child : rootAnalysisPart.getChildPart()) {
            setOidForAllAnalysisPartChildren(analysisOid + "." + increment, child);
            increment++;
        }
    }
}
