package net.ihe.gazelle.mca.contentanalyzer.adapters.analysis.dao;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.interlay.dao.FileRepository;
import net.ihe.gazelle.evsclient.interlay.dao.Repository;

public enum McaRepositoryTypes implements Repository {

    MCA("object_for_validator_detector_repository", "analysis_", ".mca"),
    MCA_LOG("object_for_validator_detector_repository", "migrationLogs", ".mca");

    private FileRepository fileRepository;

    private McaRepositoryTypes(final String key, final String filePrefix, final String fileExtension) {
        this.fileRepository = new FileRepository(key, filePrefix, fileExtension);
    }

    @Override
    public String buildFilePath(ApplicationPreferenceManager applicationPreferenceManager, String id) {
        return fileRepository.buildFilePath(applicationPreferenceManager,id);
    }

    @Override
    public String buildFilePath(ApplicationPreferenceManager applicationPreferenceManager) {
        return fileRepository.buildFilePath(applicationPreferenceManager);
    }

    @Override
    public String getRootPathPreferenceKey() {
        return fileRepository.getRootPathPreferenceKey();
    }
}
