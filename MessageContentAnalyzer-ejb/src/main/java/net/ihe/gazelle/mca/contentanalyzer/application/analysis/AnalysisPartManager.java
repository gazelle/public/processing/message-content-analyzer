package net.ihe.gazelle.mca.contentanalyzer.application.analysis;

import net.ihe.gazelle.evsclient.domain.validation.ValidationRef;
import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.AnalysisPartDao;
import net.ihe.gazelle.mca.contentanalyzer.application.utils.DocTypeUtils;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;

import javax.xml.bind.DatatypeConverter;
import java.io.Serializable;
import java.util.Map;

public class AnalysisPartManager implements Serializable {
   private static final long serialVersionUID = -4115006569616401825L;

   private final AnalysisPartDao analysisPartDao;

   public AnalysisPartManager(AnalysisPartDao analysisPartDao) {
      this.analysisPartDao = analysisPartDao;
   }

   public AnalysisPart getByOid(String oid) {
      return analysisPartDao.getByOid(oid);
   }

   public AnalysisPart save(AnalysisPart analysisPart) {
      return analysisPartDao.save(analysisPart);
   }

   public Map<String, String> getListOfNamespacesForAnalysisPart(AnalysisPart analysisPart) {
      if (analysisPart.getNamespaces() != null
            && !analysisPart.getNamespaces().isEmpty()) {
         return analysisPart.getNamespaces();
      } else {
         AnalysisPart parent = analysisPart.getParentPart();
         while (parent != null && parent.getParentPart() != null) {
            if (DocTypeUtils.isBase64(parent.getDocType())) {
               return null;
            } else if (DocTypeUtils.isXml(parent.getDocType())) {
               return parent.getNamespaces();
            }
            parent = parent.getParentPart();
         }
      }
      return null;
   }

   public boolean isXmlChild(AnalysisPart analysisPart) {
      AnalysisPart parentPart = analysisPart.getParentPart();
      String docType = analysisPart.getDocType();
      if (DocTypeUtils.isXml(docType)) {
         return true;
      } else if (analysisPart.getParentPart() != null) {
         String parentDocType = parentPart.getDocType();
         if (parentDocType != null && (DocTypeUtils.isBase64(parentDocType) ||
                 DocTypeUtils.isFile(parentDocType) ||
                 DocTypeUtils.isFolder(parentDocType))) {
            return false;
         } else if (DocTypeUtils.isXml(parentDocType)) {
            return true;
         } else if (parentDocType != null) {
            return isXmlChild(parentPart);
         }
      }
      return false;
   }

   public void recordValidation(AnalysisPart part, String ref) {
      ValidationRef validation = ValidationRef.Serialization.toValidationRef(DatatypeConverter.parseBase64Binary(ref));
      if (validation != null && part != null && !validation.equals(part.getValidation())) {
         part.setValidation(validation);
         save(part);
      }
   }

    public AnalysisPart getAnalysisPart(ValidationRef validationRef) {
      return this.analysisPartDao.getByValidation(validationRef);
    }
}
