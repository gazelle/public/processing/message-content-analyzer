package net.ihe.gazelle.mca.contentanalyzer.business.model.config;

import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.XmlTagConfigInterface;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.XmlNodeMatcher;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import javax.xml.xpath.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "tag",
        "namespace",
        "sample"
})
@XmlRootElement(name = "xmlTagConfig")
@Entity
@Table(name = "mca_xml_tag_config", schema = "public")
@SequenceGenerator(name = "mca_xml_tag_config_sequence", sequenceName = "mca_xml_tag_config_seq", allocationSize = 1)
public class XmlTagConfigEntity extends Config implements XmlTagConfigInterface {
    @XmlTransient
    @Transient
    private static final ConfigType configType = ConfigType.TAG_CONFIG;

    @XmlTransient
    @Transient
    private static Pattern xpath = Pattern.compile("^\\s*<xpath(?:\\s+tags=\"(.*?)\")?>(.*)");

    public static XmlNodeMatcher buildMatcher(String tag, String namespace) {
        XmlNodeMatcher matcher = null;
        Matcher m = xpath.matcher(tag);
        if (m.matches()) {
            try {
                matcher = new XpathMatcher(m.group(1), m.group(2),namespace);
            } catch (XPathExpressionException e) {
            }
        }
        if (tag.startsWith("<regexp>")) {
            matcher = new RegexpMatcher(tag.substring(8),namespace);
        }
        if (matcher==null) {
            if (tag.matches("[a-zA-Z1-9_]+")) {
                matcher = new PlainMatcher(tag,namespace);
            } else {
                matcher = new RegexpMatcher(tag,namespace);
            }
        }
        return matcher;
    }

    @XmlTransient
    @Id
    @NotNull
    @GeneratedValue(generator = "mca_xml_tag_config_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @XmlElement(required = true)
    @Column(name = "tag_name")
    private String tag;

    @XmlElement(required = true)
    @Column(name = "namespace")
    private String namespace;

    @XmlElement(required = true)
    @Lob
    @Type(type = "text")
    @Column(name = "sample")
    private String sample;

    @XmlTransient
    @Transient
    private XmlNodeMatcher matcher;

    ///////////////////////////////////////////////////////////////////////////////
    ////////////////////       Constructor       //////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    public XmlTagConfigEntity(final String tag, final String namespace, final String docType, final ValidationType validationType) {
        this.tag = tag;
        this.namespace = namespace;
        super.setDocType(docType);
        super.setValidationType(validationType);
    }

    public XmlNodeMatcher getMatcher() {
        if (matcher==null) {
            matcher = buildMatcher(this.tag,this.namespace);
        }
        return matcher;
    }

    public XmlTagConfigEntity() {
    }

    ///////////////////////////////////////////////////////////////////////////////
    ////////////////////    Getter and setter     /////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    @Override
    public String getTag() {
        return this.tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    @Override
    public String getNamespace() {
        return this.namespace;
    }

    public void setNamespace(String namespace) {
        if (!namespace.isEmpty()) {
            this.namespace = namespace;
        } else {
            this.namespace = null;
        }
    }

    @Override
    public String getSample() {
        return sample;
    }

    public void setSample(String sample) {
        this.sample = sample;
    }

    ///////////////////////////////////////////////////////////////////////////////
    ////////////////////      Public Methods     //////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    @Override
    public boolean hasSameDetectionElements(XmlTagConfigInterface xmlTagConfigInterface) {

        if ((xmlTagConfigInterface.getNamespace() == null && this.namespace == null)) {
            return xmlTagConfigInterface.getTag().equals(this.tag);
        } else if (this.namespace != null && xmlTagConfigInterface.getNamespace() != null) {
            return (xmlTagConfigInterface.getTag().equals(this.tag)
                    && xmlTagConfigInterface.getNamespace().equals(this.namespace));
        }
        return false;
    }

    @Override
    public ConfigType getConfigType() {
        return configType;
    }

    @Override
    public boolean isHardCoded() {
        return false;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        sb.append(this.getDocType());
        sb.append(", ");
        sb.append(this.getValidationType());
        sb.append(", ");
        sb.append(this.getTag());
        sb.append(", ");
        sb.append(this.getNamespace());

        return sb.toString();
    }
}
