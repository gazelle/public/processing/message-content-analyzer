package net.ihe.gazelle.mca.contentanalyzer.adapters.factory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.interfaces.OidGeneratorManager;
import net.ihe.gazelle.evsclient.application.interfaces.ProcessingCacheManager;
import net.ihe.gazelle.mca.contentanalyzer.application.McaApi;
import net.ihe.gazelle.mca.contentanalyzer.application.analysis.AnalysisCacheManagerImpl;
import net.ihe.gazelle.mca.contentanalyzer.application.analysis.AnalysisManager;
import net.ihe.gazelle.mca.contentanalyzer.application.analysis.AnalysisPartManager;
import net.ihe.gazelle.mca.contentanalyzer.application.analyzers.*;
import net.ihe.gazelle.mca.contentanalyzer.application.analyzers.xml.XmlAnalyzer;
import net.ihe.gazelle.mca.contentanalyzer.application.config.McaConfigManager;
import net.ihe.gazelle.mca.contentanalyzer.application.converters.Base64Converter;
import net.ihe.gazelle.mca.contentanalyzer.application.converters.DicomToTxtConverter;
import net.ihe.gazelle.mca.contentanalyzer.application.files.FilesDownloaderManager;
import net.ihe.gazelle.mca.contentanalyzer.application.files.TmpFilesManager;
import net.ihe.gazelle.metadata.application.MetadataServiceProvider;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;

@Name("mcaApplicationFactory")
@Scope(ScopeType.APPLICATION)
@AutoCreate
public class ApplicationFactory {


    @In(value ="mcaDaoFactory")
    private DaoFactory daoFactory;

    @In(value = "applicationPreferenceManager", create = true)
    private ApplicationPreferenceManager applicationPreferenceManager;

    @In(value = "oidGeneratorManager", create = true)
    private OidGeneratorManager oidGeneratorManager;

    @In(value = "metadataServiceProvider")
    MetadataServiceProvider metadataServiceProvider;

    @In(value = "xmlAnalyzer", create = true)
    private Analyzer xmlAnalyzer;

    @In(value = "b64Analyzer", create = true)
    private Analyzer b64Analyzer;

    @In(value = "contentAnalysisTypeDetector", create = true)
    private Analyzer contentAnalysisTypeDetector;

    @In(value = "mimeTypeDetector", create = true)
    private Analyzer mimeTypeDetector;

    @Factory(value = "analysisCacheManager", scope = ScopeType.PAGE)
    public ProcessingCacheManager getAnalysisCacheManager() {
        return new AnalysisCacheManagerImpl(applicationPreferenceManager, daoFactory.getAnalysisDao());
    }

    @Factory( value = "analysisManager", scope = ScopeType.APPLICATION)
    public AnalysisManager getAnalysisManager() {
        return new AnalysisManager(daoFactory.getAnalysisDao(), applicationPreferenceManager, oidGeneratorManager,
                metadataServiceProvider, getAnalysisCacheManager());
    }

    @Factory( value = "analysisPartManager", scope = ScopeType.APPLICATION)
    public AnalysisPartManager getAnalysisPartManager() {
        return new AnalysisPartManager(daoFactory.getAnalysisPartDao());
    }

    @Factory( value = "mcaConfigManager", scope = ScopeType.APPLICATION)
    public McaConfigManager getMcaConfigManager() {
        return new McaConfigManager(daoFactory.getMcaConfigDao());
    }

    @Factory(value = "xmlAnalyzer", scope = ScopeType.PAGE)
    public XmlAnalyzer getXMLAnalyzer() {
        return new XmlAnalyzer(daoFactory.getMcaConfigDao());
    }

    @Factory(value = "b64Analyzer", scope = ScopeType.PAGE)
    public Analyzer getB64Analyzer() {
        return new B64Analyzer(getXMLAnalyzer(), getContentAnalysisTypeDetector(),
                getMimeTypeDetector(), getBase64Converter());
    }

    @Factory(value = "contentAnalysisTypeDetector", scope = ScopeType.PAGE)
    public Analyzer getContentAnalysisTypeDetector() {
        return new ContentAnalysisTypeDetector(getMessageSplitter(), daoFactory.getMcaConfigDao());
    }

    @Factory(value = "messageSplitter", scope = ScopeType.PAGE)
    public Analyzer getMessageSplitter() {
        return new MessageSplitter(xmlAnalyzer, contentAnalysisTypeDetector,
                mimeTypeDetector, b64Analyzer);
    }

    @Factory(value = "mimeTypeDetector", scope = ScopeType.PAGE)
    public Analyzer getMimeTypeDetector() {
        return new MimeTypeDetector(applicationPreferenceManager ,daoFactory.getMcaConfigDao());
    }

    @Factory(value = "messageContentAnalyzer", scope = ScopeType.PAGE)
    public MessageContentAnalyzer getMessageContentAnalyzer() {
        return new MessageContentAnalyzer(getXMLAnalyzer(), getContentAnalysisTypeDetector(),
                getMimeTypeDetector(), getB64Analyzer());
    }

    @Factory(value = "tmpFilesManager", scope = ScopeType.PAGE)
    public TmpFilesManager getTmpFilesManager() {
        return new TmpFilesManager();
    }

    @Factory(value = "dicomToTxtConverter", scope = ScopeType.PAGE)
    public DicomToTxtConverter getDicomToTxtConverter() {
        return new DicomToTxtConverter(applicationPreferenceManager, getTmpFilesManager());
    }

    @Factory(value = "base64Converter", scope = ScopeType.PAGE)
    public Base64Converter getBase64Converter() {
        return new Base64Converter();
    }

    @Factory(value = "filesDownloaderManager", scope = ScopeType.PAGE)
    public FilesDownloaderManager getFilesDownloaderManager() {
        return new FilesDownloaderManager(getAnalysisPartManager(), getDicomToTxtConverter());
    }

    @Factory(value = "mcaApi", scope = ScopeType.PAGE)
    public McaApi getMcaApi() {
        return new McaApi(getAnalysisManager(), getMessageContentAnalyzer(),
                getDicomToTxtConverter(), getBase64Converter());
    }

    @Factory(value = "analyzerManager", scope = ScopeType.PAGE)
    public AnalyzerManager getAnalyzerManager() {
        return new AnalyzerManager(getMcaApi(), getAnalysisManager(),
                getAnalysisPartManager(), getMcaConfigManager());
    }

}
