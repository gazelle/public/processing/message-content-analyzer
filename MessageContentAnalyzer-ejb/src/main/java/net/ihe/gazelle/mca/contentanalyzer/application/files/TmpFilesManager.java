package net.ihe.gazelle.mca.contentanalyzer.application.files;

import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import org.apache.tika.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;

public class TmpFilesManager implements Serializable {
    private static final long serialVersionUID = -6456161604004067623L;

    private static final Logger LOGGER = LoggerFactory.getLogger(TmpFilesManager.class);
    private static final String TMP_FILE_SUFFIX = ".tmp";

    public File createTmpFileFromByte(final String fileName, final byte[] bytes) throws UnexpectedAnalysisException{
        FileOutputStream fos = null;
        File file;
        try {
            file = File.createTempFile(fileName, TMP_FILE_SUFFIX);
            fos = new FileOutputStream(file.getAbsolutePath());
            fos.write(bytes);
            fos.close();
        } catch (final IOException e) {
            LOGGER.error("Error creating temp file : ",e);
            throw new UnexpectedAnalysisException("Error creating temporary file.", e);
        } finally {
            // Always close output streams. Doing this closes
            // the channels associated with them as well.
            IOUtils.closeQuietly(fos);
        }
        return file;
    }

    public void deleteTmpFile(final File f) {
        if (f.exists()) {
            if (f.delete()) {
                LOGGER.debug("deleting file : {}", f.getName());
            } else {
                LOGGER.error("deletion operation failed.");
            }
        } else {
            LOGGER.error("Delete operation is already done!");
        }
    }
}
