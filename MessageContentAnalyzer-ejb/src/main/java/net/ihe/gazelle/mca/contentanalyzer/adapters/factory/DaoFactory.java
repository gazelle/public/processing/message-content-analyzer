package net.ihe.gazelle.mca.contentanalyzer.adapters.factory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.mca.contentanalyzer.adapters.analysis.dao.AnalysisDaoImpl;
import net.ihe.gazelle.mca.contentanalyzer.adapters.analysis.dao.AnalysisPartDaoImpl;
import net.ihe.gazelle.mca.contentanalyzer.adapters.config.dao.McaConfigDaoImpl;
import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.AnalysisDao;
import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.AnalysisPartDao;
import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.McaConfigDao;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;

import javax.persistence.EntityManagerFactory;

@Name("mcaDaoFactory")
@Scope(ScopeType.APPLICATION)
@AutoCreate
public class DaoFactory {

    @In(value = "entityManagerFactory", create = true)
    private EntityManagerFactory entityManagerFactory;

    @In(value = "applicationPreferenceManager", create = true)
    private ApplicationPreferenceManager applicationPreferenceManager;

    @Factory( value = "analysisDao")
    public AnalysisDao getAnalysisDao() {
        return new AnalysisDaoImpl(entityManagerFactory, applicationPreferenceManager);
    }

    @Factory( value = "analysisPartDao")
    public AnalysisPartDao getAnalysisPartDao() {
        return new AnalysisPartDaoImpl(entityManagerFactory);
    }

    @Factory( value = "mcaConfigDAO")
    public McaConfigDao getMcaConfigDao() {
        return new McaConfigDaoImpl(entityManagerFactory);
    }
}
