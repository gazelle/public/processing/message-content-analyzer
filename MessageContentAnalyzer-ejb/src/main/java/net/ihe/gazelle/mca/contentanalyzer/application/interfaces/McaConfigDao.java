package net.ihe.gazelle.mca.contentanalyzer.application.interfaces;

import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.ConfigsFromDB;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.*;

import java.io.Serializable;
import java.util.List;

public interface McaConfigDao extends Serializable {
    List<MimeTypeConfigInterface> getAllMimeTypeConfig();

    List<ContentAnalysisConfigInterface> getAllContentAnalysisConfig();

    List<XmlTagConfigInterface> getAllTags();

    ConfigsFromDB getConfigsFromDB();

    List<MimeTypeConfigInterface> getMimeTypeConfigWithValidationType(ValidationType validationType);

    List<ContentAnalysisConfigInterface> getContentAnalysisTypeDetectionConfigWithValidationType(ValidationType validationType);

    List<XmlTagConfigInterface> getXmlTagsWithValidationType(ValidationType validationType);

    List<ZipStructureInterface> getZipStructures();

    ZipStructureInterface getZipStructureWithDocType(String docType);

    int addNewImportedConfiguration(ConfigsFromDB configsToImport,
                                    ConfigsFromDB overwrittenConfigs);

    int removeAllConfigsFromDB();

    int addAllImportedConfigurationsToDB(ConfigsFromDB configsToImport);

    void remove(ConfigInterface selectedConfig);

    void merge(ConfigInterface selectedConfig);
}
