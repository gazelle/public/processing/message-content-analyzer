package net.ihe.gazelle.mca.contentanalyzer.adapters.config.gui;

import net.ihe.gazelle.common.filter.list.GazelleListDataModel;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.evsclient.interlay.gui.I18n;
import net.ihe.gazelle.evsclient.interlay.gui.processing.FileProcessingBeanGui;
import net.ihe.gazelle.mca.contentanalyzer.application.config.McaConfigManager;
import net.ihe.gazelle.mca.contentanalyzer.application.converters.ConfigToXmlConverter;
import net.ihe.gazelle.mca.contentanalyzer.application.files.FilesDownloaderManager;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.*;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.ConfigInterface;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.ContentAnalysisConfigInterface;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.MimeTypeConfigInterface;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.XmlTagConfigInterface;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.omg.CORBA.PUBLIC_MEMBER;

import javax.faces.context.FacesContext;
import javax.xml.bind.JAXBException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;


public class ConfigBeanGui extends FileProcessingBeanGui implements Serializable {

    private static String SHOW_ALL = "Show All";
    private static String N_A = "N/A";
    private static String CONFIG_DESCRIPTION = "mcaConfigAsXml.xml";

    private List<ConfigInterface> configs;

    private List<ValidationType> validationTypes = Arrays.asList(ValidationType.values());
    private List<ConfigType> configTypes = Arrays.asList(ConfigType.values());
    private List<String> validationTypeFilters;
    private String selectedValidationTypeFilter;
    private ConfigType filteredConfigType;

    private XmlTagConfigInterface selectedTagConfig;
    private ContentAnalysisConfigInterface selectedContentAnalysisConfig;
    private MimeTypeConfigInterface selectedMimeTypeConfig;

    private String selectedStartsWith;
    private String displayedStartsWith;
    private String selectedBytePattern;


    private int nbrDeletedConfigs = 0;
    private int nbrAddedConfigs = 0;
    private ConfigsFromDB overwrittenConfigs;

    private boolean clearAndImportConfigs;
    private boolean displayImportReport = false;

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

    private final McaConfigManager mcaConfigManager;

    private final FilesDownloaderManager filesDownloaderManager;

    ///////////////////////////////////////////////////////////////////////////////
    ////////////////////       Constructor       //////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    public ConfigBeanGui(McaConfigManager mcaConfigManager, FilesDownloaderManager filesDownloaderManager) {
        super(ConfigBeanGui.class);
        this.mcaConfigManager = mcaConfigManager;
        this.filesDownloaderManager = filesDownloaderManager;
        this.validationTypeFilters = new ArrayList<>();
        this.validationTypeFilters.add(SHOW_ALL);
        this.validationTypeFilters.add("N/A");
        for (ValidationType validationType : this.validationTypes) {
            this.validationTypeFilters.add(validationType.getValue());
        }
        listConfigs();
    }

    ///////////////////////////////////////////////////////////////////////////////
    ////////////////////    Getter and setter     /////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    public GazelleListDataModel<ConfigInterface> getConfigs() {
        if (this.configs != null) {
            return new GazelleListDataModel<ConfigInterface>(this.configs);
        } else {
            return null;
        }
    }

    public void setConfigs(List<ConfigInterface> configs) {
        if (configs != null) {
            this.configs = new ArrayList<>(configs);
        } else {
            this.configs = null;
        }
    }

    public void resetUpload() {
        super.resetUpload();
        this.clearAndImportConfigs = false;
    }

    public boolean getClearAndImportConfigs() {
        return clearAndImportConfigs;
    }

    public void setClearAndImportConfigs(boolean clearAndImportConfigs) {
        this.clearAndImportConfigs = clearAndImportConfigs;
    }

    public String getSelectedValidationTypeFilter() {
        return selectedValidationTypeFilter;
    }

    public void setSelectedValidationTypeFilter(String selectedValidationTypeFilter) {
        this.selectedValidationTypeFilter = selectedValidationTypeFilter;
        listConfigs();
    }

    public ConfigType getFilteredConfigType() {
        return this.filteredConfigType;
    }

    public List<String> getValidationTypeFilters() {
        return validationTypeFilters;
    }

    public void setFilteredConfigType(ConfigType filteredConfigType) {
        this.filteredConfigType = filteredConfigType;
        listConfigs();
    }

    public XmlTagConfigInterface getSelectedTagConfig() {
        return selectedTagConfig;
    }

    public void setSelectedTagConfig(ConfigInterface selectedTagConfig) {
        this.selectedTagConfig = (XmlTagConfigInterface) selectedTagConfig;
    }

    public ContentAnalysisConfigInterface getSelectedContentAnalysisConfig() {
        return selectedContentAnalysisConfig;
    }

    public void setSelectedContentAnalysisConfig(ConfigInterface selectedContentAnalysisConfig) {
        this.selectedContentAnalysisConfig = (ContentAnalysisConfigInterface) selectedContentAnalysisConfig;
    }

    public MimeTypeConfigInterface getSelectedMimeTypeConfig() {
        return selectedMimeTypeConfig;
    }

    public void setSelectedMimeTypeConfig(ConfigInterface selectedMimeTypeConfig) {
        this.selectedMimeTypeConfig = (MimeTypeConfigInterface) selectedMimeTypeConfig;
    }

    public ConfigInterface getSelectedConfig() {

        if (selectedContentAnalysisConfig != null) {
            return selectedContentAnalysisConfig;
        } else if (selectedMimeTypeConfig != null) {
            return selectedMimeTypeConfig;
        } else if (selectedTagConfig != null) {
            return selectedTagConfig;
        }
        return null;
    }

    public ConfigType getSelectedConfigType() {
        return getSelectedConfig().getConfigType();
    }

    public void setSelectedConfig(ConfigInterface selectedConfig) {
        setSelectedContentAnalysisConfig(null);
        setSelectedMimeTypeConfig(null);
        setSelectedTagConfig(null);
        if (selectedConfig != null) {
            ConfigType configType = selectedConfig.getConfigType();
            if (configType == ConfigType.CONTENT_ANALYSIS_CONFIGURATION) {
                setSelectedContentAnalysisConfig(selectedConfig);
                computeSelectedBytePatternToHexString();
            } else if (configType == ConfigType.MIME_TYPE_CONFIG) {
                setSelectedMimeTypeConfig(selectedConfig);
                selectedBytePattern = null;
            } else if (configType == ConfigType.TAG_CONFIG) {
                setSelectedTagConfig(selectedConfig);
                selectedBytePattern = null;
            }
        }
    }

    public String getSelectedStartsWith() {
        return selectedStartsWith;
    }

    public void setSelectedStartsWith(String selectedStartsWith) {
        this.selectedStartsWith = selectedStartsWith;
    }

    public String getDisplayedStartsWith() {
        return displayedStartsWith;
    }

    public void setDisplayedStartsWith(String displayedStartsWith) {
        this.displayedStartsWith = displayedStartsWith;
    }

    public String getSelectedBytePattern() {

        return selectedBytePattern;
    }

    public void setSelectedBytePattern(String selectedBytePattern) {
        this.selectedBytePattern = selectedBytePattern;
    }

    public List<ValidationType> getValidationTypes() {
        return validationTypes;
    }

    public List<ConfigType> getConfigTypes() {
        return configTypes;
    }

    public ConfigsFromDB getOverwrittenConfigs() {
        return overwrittenConfigs;
    }

    public List<ConfigInterface> getOverwrittenConfigsList() {

        List<ConfigInterface> overwrittenConfigsList = new ArrayList<>();
        if (this.overwrittenConfigs != null) {
            overwrittenConfigsList.addAll(this.overwrittenConfigs.getContentAnalysisConfigEntities());
            overwrittenConfigsList.addAll(this.overwrittenConfigs.getMimeTypeConfigEntities());
            overwrittenConfigsList.addAll(this.overwrittenConfigs.getXmlTagConfigEntities());
        }
        return overwrittenConfigsList;
    }

    public void setOverwrittenConfigs(ConfigsFromDB overwrittenConfigs) {
        this.overwrittenConfigs = overwrittenConfigs;
    }

    public Integer getNbrDeletedConfigs() {
        return nbrDeletedConfigs;
    }

    public void setNbrDeletedConfigs(Integer nbrDeletedConfigs) {
        this.nbrDeletedConfigs = nbrDeletedConfigs;
    }

    public Integer getNbrAddedConfigs() {
        return nbrAddedConfigs;
    }

    public void setNbrAddedConfigs(Integer nbrAddedConfigs) {
        this.nbrAddedConfigs = nbrAddedConfigs;
    }

    public boolean isDisplayImportReport() {
        return displayImportReport;
    }

    public void setDisplayImportReport(boolean displayImportReport) {
        this.displayImportReport = displayImportReport;
    }

    ///////////////////////////////////////////////////////////////////////////////
    ////////////////////      Public Methods     //////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    public void listConfigs() {

        ArrayList<ConfigInterface> configs = new ArrayList<>();
        if (this.getSelectedValidationTypeFilter() == null || this.getSelectedValidationTypeFilter().equals(SHOW_ALL)) {
            if (this.filteredConfigType != null) {
                if (this.filteredConfigType == ConfigType.CONTENT_ANALYSIS_CONFIGURATION) {
                    configs.addAll(mcaConfigManager.getAllContentAnalysisConfig());
                } else if (this.filteredConfigType == ConfigType.MIME_TYPE_CONFIG) {
                    configs.addAll(mcaConfigManager.getAllMimeTypeConfig());
                } else if (this.filteredConfigType == ConfigType.TAG_CONFIG) {
                    configs.addAll(mcaConfigManager.getAllTags());
                }
            } else {
                configs.addAll(mcaConfigManager.getAllContentAnalysisConfig());
                configs.addAll(mcaConfigManager.getAllMimeTypeConfig());
                configs.addAll(mcaConfigManager.getAllTags());
            }
        } else if (this.getSelectedValidationTypeFilter().equals(N_A)) {
            if (this.filteredConfigType != null) {
                if (this.filteredConfigType == ConfigType.CONTENT_ANALYSIS_CONFIGURATION) {
                    configs.addAll(mcaConfigManager.getContentAnalysisTypeDetectionConfigWithValidationType(null));
                } else if (this.filteredConfigType == ConfigType.MIME_TYPE_CONFIG) {
                    configs.addAll(mcaConfigManager.getMimeTypeConfigWithValidationType(null));
                } else if (this.filteredConfigType == ConfigType.TAG_CONFIG) {
                    configs.addAll(mcaConfigManager.getXmlTagsWithValidationType(null));
                }
            } else {
                configs.addAll(mcaConfigManager.getContentAnalysisTypeDetectionConfigWithValidationType(null));
                configs.addAll(mcaConfigManager.getMimeTypeConfigWithValidationType(null));
                configs.addAll(mcaConfigManager.getXmlTagsWithValidationType(null));
            }
        } else {
            if (this.filteredConfigType != null) {
                if (this.filteredConfigType == ConfigType.CONTENT_ANALYSIS_CONFIGURATION) {
                    configs.addAll(mcaConfigManager.getContentAnalysisTypeDetectionConfigWithValidationType(
                            ValidationType.valueOf(this.getSelectedValidationTypeFilter())));
                } else if (this.filteredConfigType == ConfigType.MIME_TYPE_CONFIG) {
                    configs.addAll(mcaConfigManager.getMimeTypeConfigWithValidationType(
                            ValidationType.valueOf(this.getSelectedValidationTypeFilter())));
                } else if (this.filteredConfigType == ConfigType.TAG_CONFIG) {
                    configs.addAll(mcaConfigManager.getXmlTagsWithValidationType(
                            ValidationType.valueOf(this.getSelectedValidationTypeFilter())));
                }
            } else {
                configs.addAll(mcaConfigManager.getContentAnalysisTypeDetectionConfigWithValidationType(
                        ValidationType.valueOf(this.getSelectedValidationTypeFilter())));
                configs.addAll(mcaConfigManager.getMimeTypeConfigWithValidationType(
                        ValidationType.valueOf(this.getSelectedValidationTypeFilter())));
                configs.addAll(mcaConfigManager.getXmlTagsWithValidationType(
                        ValidationType.valueOf(this.getSelectedValidationTypeFilter())));
            }
        }
        setConfigs(configs);
        setSelectedConfig(null);
        setDisplayedStartsWith(null);
        setSelectedStartsWith(null);
    }

    public void newStartsWith() {
        displayedStartsWith = "";
        selectedStartsWith = null;
    }

    public void editStartsWith() {
        displayedStartsWith = selectedStartsWith;
    }

    public void saveStartsWith() {
        if (selectedStartsWith != null) {
            if (selectedContentAnalysisConfig.getStartsWith() != null) {
                selectedContentAnalysisConfig.getStartsWith().remove(selectedStartsWith);
            }
        }
        if (displayedStartsWith != null) {
            if (selectedContentAnalysisConfig.getStartsWith() == null) {
                selectedContentAnalysisConfig.setStartsWith(new ArrayList<String>());
            }
            selectedContentAnalysisConfig.getStartsWith().add(displayedStartsWith);
            selectedStartsWith = displayedStartsWith;
            displayedStartsWith = null;
        }
    }

    public void deleteStartsWith() {
        if (selectedStartsWith != null) {
            selectedContentAnalysisConfig.getStartsWith().remove(selectedStartsWith);
        }
        selectedStartsWith = null;
        displayedStartsWith = null;
    }

    public void cancel() {
        selectedStartsWith = null;
        displayedStartsWith = null;
    }

    public void changeStartsWith() {
        if (displayedStartsWith!= null && selectedStartsWith != null && !displayedStartsWith.equals(selectedStartsWith)) {
            setDisplayedStartsWith(null);
        }
    }

    public void createNewTagConfiguration() {
        XmlTagConfigInterface tagConfig = new XmlTagConfigEntity();
        setSelectedConfig(tagConfig);
    }

    public void createNewMimeTypeConfiguration() {
        MimeTypeConfigInterface mimeTypeConfig = new MimeTypeConfigEntity();
        setSelectedConfig(mimeTypeConfig);
    }

    public void createNewContentAnalysisConfiguration() {
        ContentAnalysisConfigInterface contentAnalysisConfig = new ContentAnalysisConfigEntity();
        setSelectedConfig(contentAnalysisConfig);
        computeSelectedBytePatternToHexString();
    }

    public void deleteSelectedConfig() {
        mcaConfigManager.remove(getSelectedConfig());
        listConfigs();
    }

    public void saveSelectedConfig() {

        ConfigInterface selectedConfig = getSelectedConfig();
        if (getSelectedConfigType() == ConfigType.CONTENT_ANALYSIS_CONFIGURATION) {
            if (selectedBytePattern != null && !selectedBytePattern.equals("")) {
                if (selectedBytePattern.matches("[0-9a-fA-F]+") && selectedBytePattern.length()%2==0) {
                    selectedBytePattern = selectedBytePattern.toUpperCase(Locale.ENGLISH);
                    int len = selectedBytePattern.length();
                    byte[] bytePattern = new byte[len / 2];
                    for (int i = 0; i < len; i += 2) {
                        bytePattern[i / 2] = (byte) ((Character.digit(selectedBytePattern.charAt(i), 16) << 4)
                                + Character.digit(selectedBytePattern.charAt(i+1), 16));
                    }
                    selectedContentAnalysisConfig.setBytePattern(bytePattern);
                } else {
                    GuiMessage.logMessage(StatusMessage.Severity.ERROR,
                            "Unexpected byte pattern hex string");
                    return;
                }
            } else {
                selectedContentAnalysisConfig.setBytePattern(null);
            }
        } else if (getSelectedConfigType() == ConfigType.TAG_CONFIG) {
            for (ConfigInterface config : configs) {
                if (config.getConfigType() == ConfigType.TAG_CONFIG) {
                    if (((XmlTagConfigInterface) config).hasSameDetectionElements(((XmlTagConfigInterface) selectedConfig))
                            && ((XmlTagConfigInterface) config)!= getSelectedConfig()) {
                        GuiMessage.logMessage(StatusMessage.Severity.ERROR,
                                "This tag and namespace are already in configuration");
                        return;
                    }
                }
            }
        }

        mcaConfigManager.merge(selectedConfig);
        listConfigs();
        setSelectedConfig(selectedConfig);
        GuiMessage.logMessage(StatusMessage.Severity.INFO,
                "Configuration successfully saved.");
    }

    public void computeSelectedBytePatternToHexString() {
        if (selectedContentAnalysisConfig.getBytePattern() != null) {
            char[] hexChars = new char[selectedContentAnalysisConfig.getBytePattern().length * 2];
            for ( int j = 0; j < selectedContentAnalysisConfig.getBytePattern().length; j++ ) {
                int v = selectedContentAnalysisConfig.getBytePattern()[j] & 0xFF;
                hexChars[j * 2] = hexArray[v >>> 4];
                hexChars[j * 2 + 1] = hexArray[v & 0x0F];
            }
            this.selectedBytePattern = new String(hexChars);
        } else {
            this.selectedBytePattern = null;
        }
    }

    public boolean isTagConfigFiltered() {
        if (this.filteredConfigType != null && this.filteredConfigType == ConfigType.TAG_CONFIG) {
            return true;
        }
        return false;
    }

    public boolean isMimeTypeConfigFiltered() {
        if (this.filteredConfigType != null && this.filteredConfigType == ConfigType.MIME_TYPE_CONFIG) {
            return true;
        }
        return false;
    }

    public boolean isContentAnalysisConfigFiltered() {
        if (this.filteredConfigType != null && this.filteredConfigType == ConfigType.CONTENT_ANALYSIS_CONFIGURATION) {
            return true;
        }
        return false;
    }

    public void exportConfigsAsXml() throws JAXBException {
        ConfigToXmlConverter configToXmlConverter = new ConfigToXmlConverter();
        String xmlConfigs = configToXmlConverter.configToXml(getConfigs().getAllItems(FacesContext.getCurrentInstance()));
        filesDownloaderManager.downloadConfigs(xmlConfigs, FacesContext.getCurrentInstance(), CONFIG_DESCRIPTION);
    }



    public void resetFilter() {
        this.selectedValidationTypeFilter = null;
        this.filteredConfigType = null;
        listConfigs();
    }

    public void importConfigsFromXml() {

        ConfigToXmlConverter configToXmlConverter = new ConfigToXmlConverter();
        overwrittenConfigs = new ConfigsFromDB();

        try {
            ConfigsFromDB configsToImport = configToXmlConverter.xmlToConfig(super.getDocumentText());
            if (clearAndImportConfigs) {
                nbrDeletedConfigs = mcaConfigManager.removeAllConfigsFromDB();
                nbrAddedConfigs = mcaConfigManager.addAllImportedConfigurationsToDB(configsToImport);
                overwrittenConfigs = null;
            } else {
                nbrDeletedConfigs = 0;
                nbrAddedConfigs = mcaConfigManager.addNewImportedConfiguration(configsToImport,
                        overwrittenConfigs);
            }
        } catch (JAXBException e) {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, "Error importing configurations with message :\n\"" +
                    e.getMessage() + "\" Please check your configuration file.");
        }

        super.setUploadedFileName(null);
        this.displayImportReport = true;
        listConfigs();
    }

    public boolean checkRequiredFields(){
        ConfigInterface selectedConfig = getSelectedConfig();
        ConfigType selectedConfigType = getSelectedConfigType();
        boolean isDocTypeNull = (selectedConfig.getDocType() == null);
        if(selectedConfigType == ConfigType.TAG_CONFIG){
            String tag = ((XmlTagConfigInterface)selectedConfig).getTag();
            return (!isDocTypeNull &&  tag != null && !"".equals(tag.trim()));
        }
        if(selectedConfigType == ConfigType.MIME_TYPE_CONFIG){
            String mimeType = ((MimeTypeConfigInterface)selectedConfig).getMimeType();
            return (!isDocTypeNull &&  mimeType != null && !"".equals(mimeType.trim()));
        }
        if(selectedConfigType == ConfigType.CONTENT_ANALYSIS_CONFIGURATION){
            return !isDocTypeNull;
        }
        return false;
    }

    public String noConfigMessage(){
        return nbrAddedConfigs == 0 ? I18n.get("net.ihe.gazelle.mca.ConfigBeannbrAddedConfigsNullNoConfigurationHave")
                : (nbrAddedConfigs + I18n.get("net.ihe.gazelle.mca.ConfigurationsHaveBeenAddedToTheDatabase"));
    }
}
