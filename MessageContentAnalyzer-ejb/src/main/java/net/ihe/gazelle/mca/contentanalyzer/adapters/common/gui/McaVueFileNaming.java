package net.ihe.gazelle.mca.contentanalyzer.adapters.common.gui;

public enum McaVueFileNaming {


    DETAILED_RESULT("messageContentAnalyzerDetailedResult"),
    ANALYSIS("messageContentAnalyzer");

    private static final String SEAM_FILE_EXTENSION = ".seam";
    private static final String XHTML_FILE_EXTENSION = ".xhtml";

    private String page;

    McaVueFileNaming(String page) {
        this.page = page;
    }

    public String getLink() {
        return this.page.concat(XHTML_FILE_EXTENSION);
    }

    public String getMenuLink() {
        return this.page.concat(SEAM_FILE_EXTENSION);
    }
}
