package net.ihe.gazelle.mca.contentanalyzer.application.interfaces;

import net.ihe.gazelle.evsclient.domain.validation.ValidationRef;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;

import java.io.Serializable;

public interface AnalysisPartDao extends Serializable {

    AnalysisPart getByOid(String oid);

    AnalysisPart save(AnalysisPart analysisPart);

    AnalysisPart getByValidation(ValidationRef validation);
}
