package net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces;

import java.util.List;

public interface ContentAnalysisConfigInterface extends ConfigInterface {

    List<String> getStartsWith();

    byte[] getBytePattern();

    String getStringPattern();

    String getUnwantedContent();

    void setBytePattern(byte[] bytePattern);

    void setStartsWith(List<String> startElements);

    public String getStartsWithAsString();
}
