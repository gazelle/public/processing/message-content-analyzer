package net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces;

public interface MimeTypeConfigInterface extends ConfigInterface {

    String getMimeType();
}
