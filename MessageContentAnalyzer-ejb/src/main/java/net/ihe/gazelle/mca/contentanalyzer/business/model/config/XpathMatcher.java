package net.ihe.gazelle.mca.contentanalyzer.business.model.config;

import net.ihe.gazelle.evsclient.interlay.factory.DocumentBuilderFactoryException;
import net.ihe.gazelle.evsclient.interlay.factory.XmlDocumentBuilderFactory;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.XmlNodeMatcher;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.xpath.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;

public class XpathMatcher implements XmlNodeMatcher {
    private String namespace;
    private String expression;
    private Pattern tags;

    public XpathMatcher(String tags, String expression, String namespace) throws XPathExpressionException {
        this.namespace = namespace;
        XPath xpath = XPathFactory.newInstance().newXPath();
        this.expression=expression;
        XPathExpression xPathExpression = xpath.compile("boolean(" + expression + ")");
        this.tags = Pattern.compile(tags);
    }

    public boolean match(CharSequence content, String tag, Map<String,String> namespaces) {
        if (isPossibleMatch(tag)) {
            return match(parse(content,namespaces), tag, namespaces);
        }
        return false;
    }

    private boolean match(Element element, String tag, final Map<String,String> namespaces) {
        if (element == null) {
            return false;
        }
        try {
            XPath xpath = XPathFactory.newInstance().newXPath();
            if (namespaces != null && !namespaces.isEmpty()) {
                xpath.setNamespaceContext(new NamespaceContext() {
                    public String getNamespaceURI(String prefix) {
                        return namespaces.get(prefix);
                    }

                    public Iterator getPrefixes(String val) {
                        return null;
                    }

                    public String getPrefix(String uri) {
                        return null;
                    }
                });
            }
            XPathExpression xPathExpression = xpath.compile("boolean(" + expression + ")");
            return (boolean) xPathExpression.evaluate(element, XPathConstants.BOOLEAN);
        } catch (XPathExpressionException e) {
            return false;
        }
    }

    boolean isPossibleMatch(String tag) {
        return this.tags.matcher(tag).matches();
    }

    private Element parse(CharSequence processedContent,Map<String,String> namespaces) {
        try {
            DocumentBuilder builder = new XmlDocumentBuilderFactory()
                    .setNamespaceAware(StringUtils.isNotEmpty(this.namespace))
                    .getBuilder();
            Document document = builder.parse(new ByteArrayInputStream(format(processedContent,namespaces)));
            return document.getDocumentElement();
        } catch (SAXException | DocumentBuilderFactoryException | IOException e) {
            return null;
        }
    }
    private byte[] format(CharSequence s,Map<String, String> namespaces) {
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        sb.append("<XML ").append(serializeNamespaces(namespaces)).append(">\n");
        sb.append(s);
        sb.append("\n</XML>");
        String xml = sb.toString();
        return xml.getBytes(StandardCharsets.UTF_8);
    }
    private String serializeNamespaces(Map<String, String> namespaces) {
        StringBuilder sb = new StringBuilder();
        if (namespaces!=null) {
            for (Map.Entry<String,String> e:namespaces.entrySet()) {
                sb.append(e.getKey()).append("=\"").append(e.getValue()).append("\" ");
            }
        }
        return sb.toString();
    }

}
