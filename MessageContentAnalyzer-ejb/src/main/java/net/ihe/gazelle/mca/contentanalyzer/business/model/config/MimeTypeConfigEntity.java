package net.ihe.gazelle.mca.contentanalyzer.business.model.config;


import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.MimeTypeConfigInterface;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "mimeType",
        "sample"
})
@XmlRootElement(name = "mimeTypeConfig")
@Entity
@Table(name = "mca_mime_type_config", schema = "public",uniqueConstraints = @UniqueConstraint(columnNames = "mime_type"))
@SequenceGenerator(name = "mca_mime_type_config_sequence", sequenceName = "mca_mime_type_config_seq", allocationSize = 1)
public class MimeTypeConfigEntity extends Config implements MimeTypeConfigInterface {

    @XmlTransient
    @Id
    @NotNull
    @GeneratedValue(generator = "mca_mime_type_config_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @XmlElement(required = true)
    @Column(name = "mime_type")
    private String mimeType;

    @XmlElement(required = true)
    @Lob
    @Type(type = "text")
    @Column(name = "sample")
    private String sample;

    @XmlTransient
    @Transient
    private static final ConfigType configType = ConfigType.MIME_TYPE_CONFIG;

    ///////////////////////////////////////////////////////////////////////////////
    ////////////////////       Constructor       //////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    public MimeTypeConfigEntity(final String mimeType, final String docType, final ValidationType validationType) {
        this.mimeType = mimeType;
        super.setDocType(docType);
        super.setValidationType(validationType);
    }

    public MimeTypeConfigEntity() {
    }


    ///////////////////////////////////////////////////////////////////////////////
    ////////////////////    Getter and setter     /////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    @Override
    public String getSample() {
        return sample;
    }

    public void setSample(String sample) {
        this.sample = sample;
    }

    @Override
    public boolean isHardCoded() {
        return false;
    }

    @Override
    public ConfigType getConfigType() {
        return configType;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        sb.append(this.getDocType());
        sb.append(", ");
        sb.append(this.getValidationType());
        sb.append(", ");
        sb.append(this.getMimeType());

        return sb.toString();
    }
}
