package net.ihe.gazelle.mca.contentanalyzer.application.interfaces;

public class DecodedPartNotSavedException extends Exception{

    public DecodedPartNotSavedException(String s) {
        super(s);
    }

    public DecodedPartNotSavedException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public DecodedPartNotSavedException(Throwable throwable) {
        super(throwable);
    }
}
