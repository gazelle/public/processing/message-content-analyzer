package net.ihe.gazelle.mca.contentanalyzer.business.exceptions;

import net.ihe.gazelle.evsclient.domain.processing.UnexpectedProcessingException;

public class UnexpectedAnalysisException extends UnexpectedProcessingException {

    public UnexpectedAnalysisException(String message) {
        super(message);
    }

    public UnexpectedAnalysisException(String message, Exception e) {
        super(message, e);
    }
}
