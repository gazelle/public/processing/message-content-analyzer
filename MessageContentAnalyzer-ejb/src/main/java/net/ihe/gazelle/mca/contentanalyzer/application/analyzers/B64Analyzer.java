package net.ihe.gazelle.mca.contentanalyzer.application.analyzers;

import net.ihe.gazelle.mca.contentanalyzer.application.converters.Base64Converter;
import net.ihe.gazelle.mca.contentanalyzer.application.utils.DocTypeUtils;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;
import net.ihe.gazelle.mca.contentanalyzer.business.model.DocType;
import net.ihe.gazelle.mca.contentanalyzer.business.model.EncodedType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;

public class B64Analyzer implements Analyzer {
    private static final long serialVersionUID = 2507091754616564298L;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(B64Analyzer.class);
    private static final String PART_LOG_TYPE = "Base64 Analyzer";


    private final Analyzer xmlAnalyzer;

    private Analyzer contentAnalysisTypeDetector;

    private final Analyzer mimeTypeDetector;

    private final Base64Converter base64Converter;

    public B64Analyzer(Analyzer xmlAnalyzer, Analyzer contentAnalysisTypeDetector, Analyzer mimeTypeDetector, Base64Converter base64Converter) {
        this.xmlAnalyzer = xmlAnalyzer;
        this.contentAnalysisTypeDetector = contentAnalysisTypeDetector;
        this.mimeTypeDetector = mimeTypeDetector;
        this.base64Converter = base64Converter;
    }

    public void setContentAnalysisTypeDetector(ContentAnalysisTypeDetector contentAnalysisTypeDetector) {
        this.contentAnalysisTypeDetector = contentAnalysisTypeDetector;
    }

    @Override
    public AnalysisPart analyze(final MessageContentAnalyzer mca, final AnalysisPart parent)
            throws UnexpectedAnalysisException {

        boolean isBase64 = base64Converter.base64Detection(mca.getMessageByteContent());

        if (isBase64) {
            AnalysisPart child = new AnalysisPart(parent);
            parent.addLog(PART_LOG_TYPE,"This part has been detected as Base64 encoded");
            base64Analyze(mca.getMessageByteContent(), child, parent);
            return child;
        }
        return null;
    }

    private void base64Analyze(byte[] messageBytesContent, final AnalysisPart child, final AnalysisPart parent)
            throws UnexpectedAnalysisException {

        LOGGER.info("Decode file between {} and {}", parent.getStartOffset(), parent.getEndOffset());

        byte[] decodedByteContent = base64Converter.base64Decoding(messageBytesContent);
        parent.addLog(PART_LOG_TYPE,"Decode message content for sub-part analysis");

        final String fileDecodedContent = new String(decodedByteContent, StandardCharsets.UTF_8);

        if (DocTypeUtils.isBase64(parent.getDocType())) {
            child.setStartOffset(0);
            child.setEndOffset(messageBytesContent.length);
            child.setEncodedType(EncodedType.B64_ENCODED);
        } else {
            child.setStartOffset(parent.getStartOffset());
            child.setEndOffset(parent.getEndOffset());
            child.setEncodedType(parent.getEncodedType());
        }
        child.setDocType(DocType.BASE64.getValue());
        child.setDecodedPart(decodedByteContent);

        if (!fileDecodedContent.equals("")) {
            LOGGER.info("Restart test with decoded file : {}", fileDecodedContent);
            child.addLog(PART_LOG_TYPE,"Start analysis on decoded file");
            MessageContentAnalyzer messageContentAnalyzer = new MessageContentAnalyzer(xmlAnalyzer, contentAnalysisTypeDetector,
                    mimeTypeDetector, this);
            messageContentAnalyzer.analyzeMessageContent(decodedByteContent, child);
            parent.getChildPart().add(child);
        }
        else {
            parent.addLog(PART_LOG_TYPE,"End of analysis : decoded file is null or empty");
        }
    }
}
